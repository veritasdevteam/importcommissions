﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.IO;
using System.Collections.Specialized;

namespace CommisionImport
{
    public static class Email
    {
        public static void SendClaimDeductAndBadFileEmails()
        {
            CreateBadFileEmail();
            CreateCDFileEmail();
        }

        public static void CreateBadFileEmail()
        {
            PublicClasses.EmailTypes.BadFile bad = new();
            StaticFuncts.SetBadFilesCount();

            if (Globals.BadFileCount == 0)
            {
                // No Files To Send...
                return;
            }

            bad.EValues = getBadEmailValues();
            bad.MMessage = getBadEMailMessage(bad.EValues);
            bad.ESetup = getSetupEmailValues();

            SendBadEmail(bad);
        }

        public static void CreateCDFileEmail()
        {
            PublicClasses.EmailTypes.CDFile cd = new();
            StaticFuncts.SetClaimDeductFileCount();

            if (Globals.ClaimDeductFileCount == 0)
            {
                // No Files To Send...
                return;
            }

            cd.EValues = getCDEmailValues();
            cd.MMessage = getCDEMailMessage(cd.EValues);
            cd.ESetup = getSetupEmailValues();

            SendCDEmail(cd);
        }



        private static void SendBadEmail(PublicClasses.EmailTypes.BadFile bad)
        {
            using (var emailClient = new SmtpClient(bad.ESetup.SmtpServer, bad.ESetup.Port))
            {
                emailClient.EnableSsl = true;
                emailClient.Credentials = getCredentials(bad.ESetup);
                emailClient.Send(bad.MMessage);
                emailClient.Dispose();
            }

            moveErrorLog();
        }

        private static void SendCDEmail(PublicClasses.EmailTypes.CDFile cd)
        {
            using (var emailClient = new SmtpClient(cd.ESetup.SmtpServer, cd.ESetup.Port))
            {
                emailClient.EnableSsl = true;
                emailClient.Credentials = getCredentials(cd.ESetup);
                emailClient.Send(cd.MMessage);
                emailClient.Dispose();
            }
        }

        private static MailMessage getBadEMailMessage(PublicClasses.EmailValues ev)
        {

            MailAddress to = new MailAddress(ev.To);
            MailAddress from = new MailAddress(ev.From); 

            MailMessage mm = new();
            mm.From = from;
            mm.To.Add(to);
            if (!string.IsNullOrEmpty(ev.CC))
            {
                MailAddress cc = new MailAddress(ev.CC);
                mm.CC.Add(cc);
            }
            mm.Subject = ev.Subject;
            mm.IsBodyHtml = true;
            mm.Body = ev.Body;
            return mm;
        }

        private static MailMessage getCDEMailMessage(PublicClasses.EmailValues ev)
        {
            MailMessage mm = new();

            MailAddress from = new MailAddress(ev.From);
            mm.From = from;

            MailAddress to = new MailAddress(ev.To.Split(";")[0]);
            mm.To.Add(to);

            if (!string.IsNullOrEmpty(ev.CC))
            {
                MailAddress cc = new MailAddress(ev.CC);
                mm.CC.Add(cc);
            }
            mm.Subject = ev.Subject;
            mm.IsBodyHtml = true;
            mm.Body = ev.Body;
            return mm;
        }

        #region EmailValues

        private static PublicClasses.EmailValues getBadEmailValues()
        {
            NameValueCollection nvc = getNameValueFromConfigSection("BadFileEmailSettings");
            PublicClasses.EmailValues eValues = getEmailValues(nvc, true);
            eValues.Body = getBadFileBody(eValues);
            return eValues;
        }

        private static PublicClasses.EmailValues getCDEmailValues()
        {
            NameValueCollection nvc = getNameValueFromConfigSection("CDEmailSettings");
            PublicClasses.EmailValues eValues = getEmailValues(nvc, false);
            eValues.Body = getClaimDeductBody(eValues.CaptionText);
            return eValues;
        }

        private static PublicClasses.EmailValues getEmailValues(NameValueCollection nvc, bool isBadFile)
        {
            string[] allKeys = nvc.AllKeys;
            PublicClasses.EmailValues eValues = new();
            foreach (var key in allKeys)
            {
                string uKey = key.ToUpper();
                switch (uKey)
                {
                    case "EMAIL_SUBJECT":
                        if (nvc[key].Contains("{0}"))
                        {
                            if (isBadFile)
                            {
                                eValues.Subject = string.Format(nvc[key].Trim(), Globals.BadFileCount);
                            }
                            else
                            {
                                eValues.Subject = string.Format(nvc[key].Trim(), Globals.ClaimDeductFileCount);
                            }
                        }
                        else
                        {
                            eValues.Subject = nvc[key].Trim();
                        }
                        break;

                    case "EMAIL_TO":
                        eValues.To = nvc[key].Trim();
                        break;

                    case "EMAIL_CC":
                        eValues.CC = nvc[key].Trim();
                        break;

                    case "EMAIL_FROM":
                        eValues.From = nvc[key].Trim();
                        break;

                    case "EMAIL_TABLECAPTIONTEXT":
                        eValues.CaptionText = nvc[key].Trim();
                        break;
                }
            }

            return eValues;
        }

        private static PublicClasses.EmailSetup getSetupEmailValues()
        {

            NameValueCollection nvc = getNameValueFromConfigSection("EmailSetup");
            PublicClasses.EmailSetup es = new();
            string[] allKeys = nvc.AllKeys;
            foreach (var key in allKeys)
            {
                string uKey = key.ToUpper();
                switch (uKey)
                {
                    case "UNAME":
                        es.UserName = nvc[key];
                        break;
                    case "PWD":
                        es.Password = StaticFuncts.ReverseText(nvc[key]);
                        break;
                    case "SMTP":
                        string smtpAndPort = nvc[key];
                        es.SmtpServer = smtpAndPort.Split(",")[0];
                        es.Port = int.Parse(smtpAndPort.Split(",")[1]);
                        break;
                }

            }
            return es;
        }

        #endregion

        private static string getBadFileBody(PublicClasses.EmailValues ev)
        {
            string href = @"href = """ + Globals.BadFilePath + (char)34;
            string badFileHyperLink = @" <a " + href + ">HERE</a> ";
            badFileHyperLink = "<u>here</u>";  // remove once the hyperlink is formatted.


            string body = "There were " + Globals.BadFileCount + " Excel Files which did NOT process.";
            body += @"<br>They can be found " + badFileHyperLink + ".  The contents of the Error Log are as follows:  <br><br>";
            body += getErrorLogTable(ev);
            body += " <br><br> Please address this issue. <b>&nbsp;Thank You!</b>";
            return body;
        }

        private static string getClaimDeductBody(string captionText)
        {
            string href = @"href = """ + Globals.BadFilePath + (char)34;
            string cdFileHyperLink = @" <a " + href + ">HERE</a> ";
            cdFileHyperLink = "<u>here</u>"; // remove once the hyperlink is formatted.

            string body = "<body>There were " + Globals.ClaimDeductFileCount + " Excel File(s) which DID process, but contained one or more Commission Deductions.";
            body += "<br>They can be found " + cdFileHyperLink + ".  Below is a list of the files which need to be addressed: <br><br>";
            body += getClaimDeductFilenamesHTML(captionText);
            body += "<br><br> Please address this issue. <b>&nbsp;Thank You!</b></body>";
            return body;
        }

        private static string getErrorLogTable(PublicClasses.EmailValues ev)
        {
            string errorTable = getStyle();
            errorTable += "<table>";

            if (!string.IsNullOrEmpty(ev.CaptionText))
            {
                errorTable += @" <caption><h1>" + ev.CaptionText + "</h1></caption>";
            }

            errorTable += "<tr background-color=#94C5F0><h3><b><th>Process Date & Time</th><th>File Name</th><th>Error Message</th></h3></b></tr>";
            string rowTemplate = @"<tr><td>&nbsp;{0}&nbsp;</td><td>&nbsp;{1}&nbsp;</td><td>&nbsp;{2}&nbsp;</td></tr>"; ;
            List<String> errorLog = getErrorLogText();

            foreach (string error in errorLog)
            {
                string[] errorSplit = error.Split("|");
                errorTable += string.Format(rowTemplate, errorSplit);
            }

            errorTable += "</body></table>";
            return errorTable;
        }

        private static NameValueCollection getNameValueFromConfigSection(string section)
        {
            // AppSettingsSection can be cast to a NameValueCollection
            NameValueCollection settingCollection = (NameValueCollection)ConfigurationManager.GetSection(section);
            return settingCollection;
        }


        private static NetworkCredential getCredentials(PublicClasses.EmailSetup es)
        {
            return new System.Net.NetworkCredential(es.UserName, es.Password);
        }

        private static string getStyle()
        {
            string style = @"<style type = ""text/css"" text-align: center;> ";
            style += " table {font-family: arial, sans-serif; border-collapse: collapse; width: 75 %; border: 3px solid #000000 } ";
            style += " th { border: 2px solid #000000; text-align: center; padding: 10px; background-color: #ADD8E6; } ";
            style += " td{ border: 1px solid #000000;text-align: center; padding: 10px; } ";
            style += "tr:nth-child(even){background-color: #dddddd; } </style>";   

            return style;
        }

        private static List<string> getErrorLogText()
        {
            List<string> errorTextLines = new();
            string errorLogPath = Globals.BadFilePath + "ErrorLog.txt";
            if (File.Exists(errorLogPath))
            {              
                using (StreamReader sr = new StreamReader(errorLogPath))
                {
                    string line;
                    // Read and display lines from the file until the end of
                    // the file is reached.
                    while ((line = sr.ReadLine()) != null)
                    {
                        errorTextLines.Add(line.Trim());
                        StaticFuncts.WriteLine(line);
                    }
                }
            }

            return errorTextLines;
        }

        private static string getClaimDeductFilenamesHTML(string captionText)
        {
            string htmlFileListTable = getStyle();
            htmlFileListTable += @" <table style = ""border-style:ridge;border-width:3px;text-align:center;";
            htmlFileListTable += @" :width:50% BORDER= ""1"" class=""myLogTable"" border-collapse:collapse; > ";

            if (!string.IsNullOrEmpty(captionText))
            {
                htmlFileListTable += @" <caption><h2>" + captionText + "</h2></caption>";
            }
            htmlFileListTable += "<tr background-color=#94C5F0><b><th><h3>File Name</h3></th></b></tr>";

            string tempFileName = string.Empty;
            string rowTemplateEven = @"<tr style=""style=""text-align center;background-color:#dddddd"";><td>{0}</td></tr>";
            string rowTemplateOdd = @"<tr style=""text-align center;background-color:#FFFFFF""><td>{0}</td></tr>";

            int count = 1;
            StaticFuncts.SetCDFiles();

            foreach (string pathName in Globals.ClaimDeductFileNames)
            {
                string fileName = Path.GetFileName(pathName);
                if ((count % 2) == 0)
                {
                    tempFileName = string.Format(rowTemplateEven, fileName);
                }
                else
                {
                    tempFileName = string.Format(rowTemplateOdd, fileName);
                }

                htmlFileListTable += tempFileName;
                count++;
            }

            htmlFileListTable += "</table>";

            return htmlFileListTable;
        }

        private static void moveErrorLog()
        {
            string currentErrorLog = "ErrorLog.txt";
            string fileAppend = "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".";
            string newErrorLogFile = currentErrorLog.Replace(".", fileAppend);
            string errorLogPath = Globals.BadFilePath + currentErrorLog;
            string backupErrorDirPath = Globals.BadFilePath + "EmailSent\\";
            if (File.Exists(errorLogPath))
            {
                if (!Directory.Exists(backupErrorDirPath))
                {
                    Directory.CreateDirectory(backupErrorDirPath);
                }

                File.Move(errorLogPath, backupErrorDirPath + newErrorLogFile);
            }
        }
    }
}
