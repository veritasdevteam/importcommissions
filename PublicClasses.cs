﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Net.Mail;


namespace CommisionImport
{
    public class PublicClasses
    {
        [Serializable]
        public class Objects_XL
        {
            public string SheetAgent { get; set; }
            public int? SheetAgentID { get; set; }
            public string MGA { get; set; }
            public int? MGA_ID { get; set; }
            public string AgentName { get; set; }
            public int? AgentID { get; set; }
            public string DealerName { get; set; }
            public string DealerNo { get; set; }
            public int? DealerID { get; set; }
            public int? ContractID { get; set; }
            public string ContractNo { get; set; }
            public string CustName { get; set; }
            public DateTime? SaleDate { get; set; }
            public DateTime? FundedDate { get; set; }
            public decimal FundingAmt { get; set; }
            public decimal Amt { get; set; }
            public DateTime? EffCancelDate { get; set; }
            public int? DaysActive { get; set; }
            public decimal DealerCost { get; set; }
            public decimal CancelPercent { get; set; }
            public string ProductType { get; set; }

            // Not Column/Row Data Fields
            public DateTime? CycleDate { get; set; }
            public string FileType { get; set; }
            public string FileName { get; set; }
            public bool IsCancelled { get; set; }
            public bool IsFlorida { get; set; }
            public bool IsValid { get; set; }
            public bool IsANCB { get; set; }          
            public bool IsOverfund { get; set; }
        }

        public class BooleanValues
        {
            public bool HasMgaColumn { get; set; }
            public bool HasAgentColumn { get; set; }
            public bool HasProductTypeColumn { get; set; }
        }

        public class ColumnValues
        { 
            public int MGA_Col { get; set; }
            public int AgentName_Col { get; set; }
            public int DealerName_Col { get; set; }
            public int DealerNo_Col { get; set; }
            public int ContractNo_Col { get; set; }
            public int CustName_Col { get; set; }
            public int SaleDate_Col { get; set; }
            public int FundedDate_Col { get; set; }
            public int CancelledFundedDate_Col { get; set; }
            public int FundingAmt_Col { get; set; }
            public int Amt_Col { get; set; }
            public int EffCancelDate_Col { get; set; }
            public int DealerCost_Col { get; set; }
            public int CancelPercent_Col { get; set; }
            public int DaysActive_Col { get; set; }
        }

        public class EmailSetup
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string SmtpServer { get; set; }
            public int Port { get; set; }
        }
        public class EmailValues
        {
            public string Subject { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public string CC { get; set; }
            public string Body { get; set; }
            public string CaptionText { get; set; }
        }

        public class EmailTypes
        {
            public class BadFile
            {
                public MailMessage MMessage { get; set; }
                public EmailValues EValues { get; set; }
                public EmailSetup ESetup { get; set; }

                public SmtpClient EmailClient = new SmtpClient();
            }

            public class CDFile
            {
                public MailMessage MMessage { get; set; }
                public EmailValues EValues { get; set; }
                public EmailSetup ESetup { get; set; }

                public SmtpClient EmailClient = new SmtpClient();
            }
        }


        public static ColumnValues GetColumnValues(int numOfCols, PublicClasses.BooleanValues bv)
        {
            bool isCorbet = Globals.FileNameProcessing.ToUpper().Contains("CORBET");
            bool isMotoRefi = Globals.FileNameProcessing.ToUpper().Contains("MOTOREFI");
            bool isPeak = Globals.FileNameProcessing.ToUpper().Contains("PEAK ");
            bool isTasaKY = Globals.FileNameProcessing.ToUpper().Contains("T.A.S.A OF KY, INC (CALL CENTER)");
            bool isPlatiumLevel = Globals.FileNameProcessing.ToUpper().Contains("PLATINUM LEVEL");
            PublicClasses.ColumnValues colValue = new();
            switch (numOfCols)
            {
                case 8:

                    if (isMotoRefi)
                    {
                        colValue.DealerNo_Col = 0;
                        colValue.DealerName_Col = 1;
                        colValue.ContractNo_Col = 2;
                        colValue.CustName_Col = 3;
                        colValue.SaleDate_Col = 4;
                        colValue.FundedDate_Col = 5;
                        colValue.FundingAmt_Col = 6;
                        colValue.Amt_Col = 7;

                    }                  
                    else
                    {
                        colValue.AgentName_Col = 0;                       
                        colValue.ContractNo_Col = 1;
                        colValue.CustName_Col = 2;
                        colValue.SaleDate_Col = 3;
                        colValue.EffCancelDate_Col = 4;
                        colValue.FundedDate_Col = 4;
                        colValue.DealerCost_Col = 5;
                        colValue.FundingAmt_Col = 5;
                        colValue.Amt_Col = 6;
                        colValue.CancelPercent_Col = 7;

                        colValue.DealerName_Col = -1;
                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                        break;
                    }
                    break;

                case 9:
                    if (isTasaKY)
                    {
                        colValue.DealerNo_Col = 0;
                        colValue.ContractNo_Col = 1;
                        colValue.CustName_Col = 2;
                        colValue.SaleDate_Col = 3;

                        colValue.EffCancelDate_Col = 4;
                        colValue.FundedDate_Col = 4;
                        colValue.FundingAmt_Col = 5;
                        colValue.DealerCost_Col = 6;
                        colValue.Amt_Col = 6;
                        colValue.CancelPercent_Col = 7;

                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }                
                    else
                    {
                        colValue.DealerNo_Col = 0;
                        colValue.DealerName_Col = 1;
                        colValue.ContractNo_Col = 2;
                        colValue.CustName_Col = 3;
                        colValue.SaleDate_Col = 4;
                        colValue.FundedDate_Col = 5;
                        colValue.FundingAmt_Col = 6;
                        colValue.DealerCost_Col = 6;
                        colValue.Amt_Col = 7;
                        colValue.CancelPercent_Col = 8;

                        colValue.EffCancelDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    break;

                case 10:
                    if (bv.HasAgentColumn & bv.HasMgaColumn)
                    {
                        colValue.MGA_Col = 0;
                        colValue.AgentName_Col = 1;
                        colValue.DealerNo_Col = 2;
                        colValue.CustName_Col = 3;
                        colValue.DealerName_Col = 3;
                        colValue.ContractNo_Col = 4;
                        colValue.FundedDate_Col = 5;
                        colValue.FundingAmt_Col = 6;
                        colValue.SaleDate_Col = 6;
                        colValue.EffCancelDate_Col = 7;
                        colValue.DealerCost_Col = 8;
                        colValue.Amt_Col = 9;
                        colValue.CancelPercent_Col = 10;                      
                 
                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    else if (bv.HasAgentColumn)
                    {
                        colValue.AgentName_Col = 0;
                        colValue.DealerNo_Col = 1;
                        colValue.DealerName_Col = 2;
                        colValue.ContractNo_Col = 3;
                        colValue.CustName_Col = 4;                        
                        colValue.SaleDate_Col = 5;
                        colValue.FundedDate_Col = 6;
                        colValue.FundingAmt_Col = 7;
                        colValue.DealerCost_Col = 7;
                        colValue.Amt_Col = 8;
                        colValue.CancelPercent_Col = 9;

                        colValue.EffCancelDate_Col = -1;
                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    else if (bv.HasMgaColumn)
                    {
                        // Not come across one of these yet.
                    }
                    else // Neither Agent nor MGA columns
                    {
                        colValue.Amt_Col = 7;
                        colValue.CancelPercent_Col = 8;
                        colValue.ContractNo_Col = 2;
                        colValue.CustName_Col = 3;
                        colValue.DealerCost_Col = 6;
                        colValue.DealerName_Col = 1;
                        colValue.DealerNo_Col = 0;
                        colValue.EffCancelDate_Col = 7;
                        colValue.FundedDate_Col = 5;
                        colValue.FundingAmt_Col = 6;
                        colValue.SaleDate_Col = 4;
                        colValue.CancelledFundedDate_Col = 5;
                        colValue.DaysActive_Col = -1;
                    }
                    break;

                case 11:

                    if (bv.HasMgaColumn)
                    {
                        colValue.MGA_Col = 0;
                        colValue.AgentName_Col = 1;
                        colValue.Amt_Col = 9;
                        colValue.CancelPercent_Col = 10;
                        colValue.ContractNo_Col = 4;
                        colValue.CustName_Col = 5;
                        colValue.DealerCost_Col = 8;
                        colValue.DealerName_Col = 3;
                        colValue.DealerNo_Col = 2;
                        colValue.EffCancelDate_Col = 7;
                        colValue.FundedDate_Col = 7;
                        colValue.FundingAmt_Col = 8;
                        colValue.SaleDate_Col = 6;
                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    else if (bv.HasMgaColumn & bv.HasAgentColumn)
                    {
                        colValue.MGA_Col = 0;
                        colValue.AgentName_Col = 1;
                        colValue.Amt_Col = 9;
                        colValue.CancelPercent_Col = 10;
                        colValue.ContractNo_Col = 4;
                        colValue.CustName_Col = 5;
                        colValue.DealerCost_Col = 8;
                        colValue.DealerName_Col = 3;
                        colValue.DealerNo_Col = 2;
                        colValue.EffCancelDate_Col = 7;
                        colValue.FundedDate_Col = 7;
                        colValue.FundingAmt_Col = 8;
                        colValue.SaleDate_Col = 6;
                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    else if (bv.HasAgentColumn)
                    {
                        colValue.AgentName_Col = 0;
                        colValue.Amt_Col = 8;
                        colValue.CancelPercent_Col = 9;
                        colValue.ContractNo_Col = 3;
                        colValue.CustName_Col = 4;
                        colValue.DealerCost_Col = 7;
                        colValue.DealerName_Col = 2;
                        colValue.DealerNo_Col = 1;
                        colValue.EffCancelDate_Col = -1;
                        colValue.FundedDate_Col = 6;
                        colValue.FundingAmt_Col = 7;
                        colValue.SaleDate_Col = 5;
                        colValue.CancelledFundedDate_Col = 6;
                        colValue.DaysActive_Col = 10;
                    }
                    else if (isCorbet)
                    {
                        colValue.DealerNo_Col = 0;
                        colValue.ContractNo_Col = 1;
                        colValue.CustName_Col = 2;
                        colValue.SaleDate_Col = 3;
                        colValue.FundedDate_Col = 4;
                        colValue.FundingAmt_Col = 5;
                        colValue.Amt_Col = 6;

                        //colValue.DealerCost_Col = 6;
                        // colValue.EffCancelDate_Col = 5;
                        //colValue.CancelPercent_Col = 8;

                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    else if (isPeak)
                    {
                        colValue.DealerNo_Col = 0;
                        colValue.DealerName_Col = 1;
                        colValue.ContractNo_Col = 2;
                        colValue.CustName_Col = 3;
                        colValue.SaleDate_Col = 4;
                        colValue.FundedDate_Col = 5;
                        colValue.FundingAmt_Col = 6;
                        colValue.Amt_Col = 7;
                        colValue.EffCancelDate_Col = 5;
                        colValue.CancelPercent_Col = 8;

                        colValue.DealerCost_Col = -1;
                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    else if (isPlatiumLevel)
                    {
                        colValue.DealerNo_Col = 0;
                        colValue.DealerName_Col = 1;
                        colValue.ContractNo_Col = 2;
                        colValue.CustName_Col = 3;
                        colValue.SaleDate_Col = 4;
                        colValue.FundedDate_Col = 5;
                        colValue.FundingAmt_Col = 6;
                        colValue.Amt_Col = 7;
                        colValue.EffCancelDate_Col = 5;
                        colValue.CancelPercent_Col = 8;

                        colValue.DealerCost_Col = -1;
                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }



                    else // Does not have MGA nor AgentName columns
                    {
                        colValue.DealerNo_Col = 0;
                        colValue.DealerName_Col = 1;
                        colValue.ContractNo_Col = 2;
                        colValue.CustName_Col = 3;
                        colValue.SaleDate_Col = 4;
                        colValue.EffCancelDate_Col = 5;
                        colValue.DealerCost_Col = 6;
                        colValue.Amt_Col = 7;
                        colValue.FundedDate_Col = 7;
                        colValue.FundingAmt_Col = 8;
                        colValue.CancelPercent_Col = 8;

                        colValue.CancelledFundedDate_Col = -1;
                        colValue.DaysActive_Col = -1;
                    }
                    break;
                case 12:
                    if (bv.HasMgaColumn & bv.HasAgentColumn)
                    {
                        colValue.MGA_Col = 0;
                        colValue.AgentName_Col = 1;
                        colValue.DealerNo_Col = 2;
                        colValue.DealerName_Col = 3;
                        colValue.ContractNo_Col = 4;
                        colValue.CustName_Col = 5;
                        colValue.SaleDate_Col = 6;
                        colValue.FundedDate_Col = 7;
                        colValue.CancelledFundedDate_Col = 7;
                        colValue.FundedDate_Col = 7;
                        colValue.FundingAmt_Col = 8;
                        colValue.DealerCost_Col = 8;
                        colValue.Amt_Col = 9;
                        colValue.CancelPercent_Col = 10;

                        colValue.EffCancelDate_Col =-1;                   
                        colValue.DaysActive_Col = -1;
                    }

                    break;
                case 13:

                    if (bv.HasMgaColumn & bv.HasAgentColumn)
                    {
                        colValue.MGA_Col = 0;
                        colValue.AgentName_Col = 1;
                        colValue.Amt_Col = 9;
                        colValue.CancelPercent_Col = 10;
                        colValue.ContractNo_Col = 4;
                        colValue.CustName_Col = 5;
                        colValue.DealerCost_Col = 8;
                        colValue.DealerName_Col = 3;
                        colValue.DealerNo_Col = 2;
                        colValue.EffCancelDate_Col = -1;
                        colValue.FundedDate_Col = 7;
                        colValue.FundingAmt_Col = 8;
                        colValue.SaleDate_Col = 6;
                        colValue.CancelledFundedDate_Col = 7;
                        colValue.DaysActive_Col = -1;
                    }
                    break;
            }

            return colValue;
        }

        public static ColumnValues SetRowZeroDealerNameColumnIndexes()
        {
            ColumnValues colValue = new();

            colValue.Amt_Col = 6;
            colValue.CancelPercent_Col = 7;
            colValue.ContractNo_Col = 1;
            colValue.CustName_Col = 2;
            colValue.DealerCost_Col = 5;
            colValue.DealerName_Col = 0;
            colValue.DealerNo_Col = -1;
            colValue.EffCancelDate_Col = 4;
            colValue.FundedDate_Col = 4;
            colValue.FundingAmt_Col = 5;
            colValue.SaleDate_Col = 3;
            colValue.CancelledFundedDate_Col = -1;
            colValue.DaysActive_Col = -1;
            return colValue;
        }
    }
}

