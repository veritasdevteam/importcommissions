﻿using System.Configuration;
using System;
using System.Data;
using System.Collections.Generic;
using CommisionImport.SQL;

namespace CommisionImport
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {               
                if (ProcessFiles.ProcessXLFiles() && Globals.SendEmail)
                {
                    Email.SendClaimDeductAndBadFileEmails();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("______________");
                Console.WriteLine(ex.StackTrace);
               // Console.ReadKey();
            }

            Console.WriteLine("Finished...");
           // Console.ReadKey();

        }

        // These are fixes from earlier, may be needed later.
        private static void UpdateMissingData()
        {
            Globals.Initialize();
            SQL_Update su = new();
            su.UpdateSaleDate();   // Sets NULL SaleDates from the Contract table
            su.UpdateDaysActive(); // Updates DaysActive with EffCancel - Sale dates
            su.UpdateAgentNames(); // Fixes NULL AgentNames           
        }
    }
}
