﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DBO;
using CommisionImport.SQL;

namespace CommisionImport.SQL
{
    class SQL_Insert : SQL_Master
    {
        string SqlTemplate = "";

        public bool InsertIntoTable_ObjectList(List<PublicClasses.Objects_XL> lco)
        {
            Console.WriteLine("InsertIntoTable_ObjectList(List<PublicClasses.Objects_XL> lco)");
           
            try
            {
                MasterSql = "Select * from  CommissionSheet";
                if (OpenDB(false))
                {
                    foreach (PublicClasses.Objects_XL ob in lco)
                    {
                        if (checkForNullAgent(ob))
                        {
                            ob.AgentID = Agent.GetAgentIDByAllMeans(ob);
                            if (Globals.IsBadFile)
                            {
                                return false;
                            }
                        }
                        SQLDB.NewRow();
                        mergeDataWithSql(ob);
                        if (Globals.IsBadFile)
                        {
                            return false;
                        }
                        RunSQL();
                    }
                    SaveDB();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
                if (Globals.IsTest)
                {
                    Console.ReadKey();
                }
                return false;
            }
            return true;
        }

        private void mergeDataWithSql(PublicClasses.Objects_XL ob)
        {
            if (!ob.AgentID.HasValue)
            {
                int? agentID = Agent.GetAgentIDByAllMeans(ob);
                if (Globals.IsBadFile)
                {
                    return;
                }
            }
            setSqlTemplate();
            string[] args = getArgs(ob);
            MasterSql = string.Format(SqlTemplate, args);
            MasterSql = MasterSql.Replace("''", "NULL");
            MasterSql = MasterSql.Replace(", ,", ",NULL,");
            return;
        }

        private void setSqlTemplate()
        {
            // AgentID 0, AgentName 1, ContractID 2, ContractNo 3, CustName 4, SaleDate 5, FundedDate 6, FundingAmt 7, Amt 8, DealerID 9 , DealerNo 10
            //  DealerName 11, EffCancelDate 12, DealerCost 13, CancelPercent 14, CycleDate 15, FileType 16, FileName 17, IsCancelled 18, isFlorida 19,
            //  isValid 20, MGA 21, MGA_ID 22, SheetAgent 23, SheetAgentID 24, DaysActive 25, IsANCB = 26, isOverfund = 27;
            SqlTemplate = "insert into CommissionSheet (";
            SqlTemplate += "AgentID, AgentName, ContractID, ContractNo, CustName, SaleDate, FundedDate, FundingAmt, Amt, DealerID, DealerNo, ";
            SqlTemplate += " DealerName, EffCancelDate, DealerCost, CancelPercent, CycleDate, FileType, FileName, IsCancelled, isFlorida, isValid, MGA, MGA_ID,  SheetAgentID, SheetAgent, DaysActive, IsANCB, isOverfund, ProductType ) ";
            SqlTemplate += "VALUES ( {0}, '{1}', {2}, '{3}', '{4}', '{5}', '{6}', {7}, {8}, {9}, '{10}', '{11}', '{12}', {13}, {14}, '{15}', '{16}', '{17}', {18}, {19}, {20}, '{21}', {22}, {23}, '{24}', {25}, {26}, {27}, '{28}')";
        }

        private string[] getArgs(PublicClasses.Objects_XL ob)
        {
            SQL_Select ss = new();
            string contractID = ConvertEmptyStringToNull(ob.ContractID.ToString());
            List<string> args = new();

            // AgentID, AgentName, ContractID, ContractNo, CustName, SaleDate, FundedDate, FundingAmt, Amt, DealerID, DealerNo, ";
            args.Add(ob.AgentID.ToString()); //0
            if (string.IsNullOrEmpty(ob.AgentName))
            {
                ob.AgentName = ss.GetAgentNameByAgentID(ob.AgentID);
            }

            args.Add(ob.AgentName); //1
            args.Add(contractID); //2
            args.Add(ob.ContractNo); //3
            args.Add(ob.CustName); //4
            if (string.IsNullOrEmpty(ob.SaleDate.ToString()))
            {
                int cID = int.Parse(contractID);
                DateTime? saleDate = ss.GetSaleDateFromContract(cID);
                args.Add(saleDate.ToString());
            }
            else
            {
                args.Add(ob.SaleDate.ToString()); //5
            }
            args.Add(ob.FundedDate.ToString()); //6
            args.Add(ob.FundingAmt.ToString()); //7
            args.Add(ob.Amt.ToString()); //8
            args.Add(ob.DealerID.ToString()); //9
            args.Add(ob.DealerNo); //10

            //  DealerName, EffCancelDate, DealerCost, CancelPercent, CycleDate, FileType, FileName,
            args.Add(ob.DealerName); //11
            args.Add(ob.EffCancelDate.ToString()); //12
            args.Add(ob.DealerCost.ToString()); //13
            args.Add(ob.CancelPercent.ToString()); //14
            args.Add(ob.CycleDate.ToString()); //15
            args.Add(ob.FileType); //16
            args.Add(ob.FileName); //17    


            // IsCancelled, isFlorida, isValid
            int isCancelled = ConvertBoolToInt(ob.IsCancelled);
            args.Add(isCancelled.ToString()); //18

            int isFlorida = ConvertBoolToInt(ob.IsFlorida);
            args.Add(isFlorida.ToString()); //19

            int isValid = ConvertBoolToInt(ob.IsValid);
            args.Add(isValid.ToString()); //20           

            //  MGA, MGA_ID, SheetAgent, SheetAgentID, DaysActive
            args.Add(ob.MGA); //21
            args.Add(ob.MGA_ID.ToString()); //22

            object sheetAgentID = ConvertEmptyStringToNull(ob.SheetAgentID.ToString());
            args.Add(sheetAgentID.ToString()); //23        
            args.Add(ob.SheetAgent); //24

            object daysActive = ConvertEmptyIntToNull(ob.DaysActive);
            if (ob.DaysActive == null || ob.DaysActive == 0)
            {
                ob.DaysActive = getDaysActive(ob);
            }
            args.Add(daysActive.ToString()); //25

            string ancb = ConvertBoolToInt(ob.IsANCB).ToString();
            if (string.IsNullOrEmpty(ancb))
            {
                ancb = "0";
            }
            args.Add(ancb); // 26
        

            string overfund = ConvertBoolToInt(ob.IsOverfund).ToString();
            if (string.IsNullOrEmpty(overfund))
            {
                overfund = "0";
            }
            args.Add(overfund); // 27
          
            args.Add(ob.ProductType); // 28



            List<string> temp = new();
            int counter = 0;
            string sValue = string.Empty;
            foreach (string arg in args)
            {
                sValue = arg;

                if (!string.IsNullOrEmpty(arg))
                {
                    if (counter != 1)
                    {
                        sValue = CleanString(arg);
                    }
                    else
                    {
                        sValue = CleanString(arg, false);
                    }
                }
                else
                {
                    sValue = DBNull.Value.ToString();
                }
                counter++;
                temp.Add(sValue);
            }

            return temp.ToArray();
        }

        private int? getDaysActive(PublicClasses.Objects_XL ob)
        {
            int? daysActive = null;
            DateTime? sd = ob.SaleDate;
            DateTime? cd = ob.EffCancelDate;
            if (sd.HasValue && cd.HasValue)
            {
                long diff = sd.Value.Ticks - cd.Value.Ticks;
                daysActive = new TimeSpan(diff).Days;
            }
            return daysActive;
        }

        private bool checkForNullAgent(PublicClasses.Objects_XL ob)
        {
            if (ob.AgentID.HasValue)
            {
                if (ob.AgentID > 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}

