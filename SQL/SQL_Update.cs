﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommisionImport.SQL
{
    class SQL_Update : SQL_Master
    {
        public void UpdateDaysActive()
        {
            SQL_Master sm = new();
            sm.MasterSql = "select * from CommissionSheet where DaysActive is null and IsCancelled = 1;";
            sm.OpenDB();
            long rowCount = sm.GetRowCount();
            for (int x = 0; x < rowCount; x++)
            {
                sm.SQLDB.GetRowNo(x);
                int id = int.Parse(sm.SQLDB.get_Fields("CommissionSheetID"));
                string sale = sm.SQLDB.get_Fields("SaleDate");
                if (!string.IsNullOrEmpty(sale))
                {
                    DateTime saleDate = DateTime.Parse(sale);
                    if (!string.IsNullOrEmpty(sm.SQLDB.get_Fields("EffCancelDate")))
                    {
                        DateTime cancelDate = DateTime.Parse(sm.SQLDB.get_Fields("EffCancelDate"));
                        double daysActive = (cancelDate - saleDate).TotalDays;
                        string sql = "Update CommissionSheet Set DaysActive = " + daysActive;
                        sql += " where CommissionSheetID = " + id;
                        sm.MasterSql = sql;
                        sm.RunSQL();
                    }
                }
            }
            sm.SQLDB.SaveDB();
        }

        public void UpdateSaleDate()
        {
            SQL_Master sm = new();
            SQL_Select ss = new();
            sm.MasterSql = "select * from CommissionSheet where SaleDate is null";
            sm.OpenDB();
            long rowCount = sm.GetRowCount();
            for (int x = 0; x < rowCount; x++)
            {
                sm.SQLDB.GetRowNo(x);
                int id = int.Parse(sm.SQLDB.get_Fields("CommissionSheetID"));
                string contractID = sm.SQLDB.get_Fields("ContractID");
                if (!string.IsNullOrEmpty(contractID))
                {
                    string saleDate = ss.GetSaleDateFromContract(int.Parse(contractID)).ToString();
                    if (!string.IsNullOrEmpty(saleDate))
                    {
                        string sql = "Update CommissionSheet Set SaleDate = '" + saleDate + "'";
                        sql += " where CommissionSheetID = " + id;
                        sm.MasterSql = sql;
                        sm.RunSQL();
                    }
                }
            }
            sm.SQLDB.SaveDB();
        }

        public void UpdateAgentNames()
        {
            SQL_Master sm = new();
            SQL_Select ss = new();
            sm.MasterSql = "select * from CommissionSheet where AgentName is null";
            sm.OpenDB();
            long rowCount = sm.GetRowCount();
            for (int x = 0; x < rowCount; x++)
            {
                sm.SQLDB.GetRowNo(x);
                int id = int.Parse(sm.SQLDB.get_Fields("CommissionSheetID"));
                string agentID = sm.SQLDB.get_Fields("AgentID");

                string agentName = ss.GetAgentNameByAgentID(int.Parse(agentID)).ToString();
                if (!string.IsNullOrEmpty(agentID))
                {
                    string sql = "Update CommissionSheet Set AgentName = '" + agentName + "'";
                    sql += " where CommissionSheetID = " + id;
                    sm.MasterSql = sql;
                    sm.RunSQL();
                }
            }
            sm.SQLDB.SaveDB();
        }
    }
}
