﻿using DBO;
using System;

namespace CommisionImport.SQL
{
    public class SQL_Master
    {
        public clsDBO SQLDB = new clsDBO();
        public string MasterSql { get; set; }

        public bool OpenDB(bool checkRows = false)
        {          
            SQLDB.OpenDB(MasterSql, Globals.ConnString);
            try
            {
                if (checkRows)
                {
                    if (SQLDB.RowCount > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }   


        public bool HasRows()
        {
            return (SQLDB.RowCount > 0);
        }

        public long GetRowCount()
        {
            long count = 0;
            try
            {
                count = SQLDB.RowCount;
            }
            catch(Exception ex)
            {
                Console.WriteLine("SQL: GetRowCount() + error = " + ex.Message);
            }

            return count;

        }

        public void GetRowNo(int rowNum)
        {
            SQLDB.GetRowNo(rowNum);
        }

        public void GetRow()
        {
            SQLDB.GetRow();
        }

        public bool RunSQL()
        {
            try
            {
               
               SQLDB.RunSQL(MasterSql, Globals.ConnString);
              
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
        public bool SaveDB()
        {
            try
            {
                SQLDB.SaveDB();
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
                return false;
            }
            return true;
        }

        public int ConvertBoolToInt(bool value)
        {
            return Convert.ToInt32(value);
        }

        public string ConvertEmptyStringToNull(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return "NULL";
            }
            return value;
        }

        public string ConvertEmptyIntToNull(int? value)
        {
            if (!value.HasValue)
            {
                return "NULL";
            }
            return value.ToString();
        }

        public string CleanString(string toClean, bool removeCommas = true)
        {
            if (!string.IsNullOrEmpty(toClean))
            {
                toClean = toClean.Replace("'", "");
                if (removeCommas)
                {
                    toClean = toClean.Replace(",", "");
                }
            }
            return toClean;
        }
    }
}
