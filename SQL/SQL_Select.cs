﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommisionImport.SQL;
using DBO;

namespace CommisionImport.SQL
{
    public class SQL_Select : SQL_Master
    {        
        public bool IsDupeRecord(string fileName)
        {          
            fileName = CleanString(fileName);
            SQL_Master sm = new();
            sm.MasterSql = "Select FileName from CommissionSheet where FileName = '" + fileName.Trim() + "'";
            bool isDupe = sm.OpenDB(true); 
            return isDupe;
        }


        #region Dealers
        public int? GetDealerIDByDealerNum(string dealerNum)
        {
            int? dealerID = null;
            dealerNum = CleanString(dealerNum);
            SQL_Master sm = new();
            sm.MasterSql = "Select DealerID from Dealer where DealerNO = '" + dealerNum + "'";
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerID = int.Parse(sm.SQLDB.get_Fields("DealerID"));
            }
            return dealerID;
        }
        public int? GetDealerIDFromDealerNo(string dealerNum)
        {
            int? dealerID = null;
            dealerNum = CleanString(dealerNum);
            SQL_Master sm = new();
            sm.MasterSql = "Select DealerID from Dealer where DealerNo = '" + dealerNum + "'";
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerID = int.Parse(sm.SQLDB.get_Fields("DealerID"));
            }
            return dealerID;
        }
        public int? GetDealerIDByDealerNameAndIsAutoNation(string dealerName, bool isAutoNation)
        {
            int? dealerID = null;
            dealerName = CleanString(dealerName.Trim());
            SQL_Master sm = new();
            int autoNationBool = ConvertBoolToInt(isAutoNation);
            sm.MasterSql = "Select DealerID from Dealer where DealerName = '" + dealerName + "' and AutoNationDealer = " + autoNationBool;
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerID = int.Parse(sm.SQLDB.get_Fields("DealerID"));
            }
            return dealerID;
        }
        public int? GetDealerIDFromContractID(int? contractID)
        {
            int? dealerID = null;
            SQL_Master sm = new();

            sm.MasterSql = "Select DealerID from Contract where ContractID = " + contractID;
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerID = int.Parse(sm.SQLDB.get_Fields("DealerID"));
            }
            return dealerID;

        }
        public int? GetDealerIDByDealerName(string dealerName)
        {
            int? dealerID = null;
            if (string.IsNullOrEmpty(dealerName))
            {
                return null;
            }

            dealerName = CleanString(dealerName);

            dealerName = dealerName.Trim();
            SQL_Master sm = new();

            sm.MasterSql = "Select DealerID from Dealer where DealerName = '" + dealerName + "'";
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerID = int.Parse(sm.SQLDB.get_Fields("DealerID"));
            }
            return dealerID;
        }
        public string GetDealerNameFromDealerID(int? dealerID)
        {
            string dealerName = string.Empty;
            SQL_Master sm = new();
            sm.MasterSql = "Select DealerName from Dealer where DealerID = " + dealerID;
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerName = sm.SQLDB.get_Fields("DealerName");
            }
            return dealerName;
        }
        public string GetDealerNoFromDealerID(int? dealerID)
        {
            string dealerNo = string.Empty;
            SQL_Master sm = new();
            MasterSql = "Select DealerNo from Dealer where DealerID = " + dealerID;
            if (sm.OpenDB(true))
            {
                SQLDB.GetRow();
                dealerNo = SQLDB.get_Fields("DealerNo");
            }
            return dealerNo;
        }
        public string GetDealerNoByDealerName(string dealerName)
        {
            dealerName = CleanString(dealerName);
            string dealerNum = String.Empty;
            SQL_Master sm = new();
            sm.MasterSql = "Select DealerNo from Dealer where DealerName =  '" + dealerName + "'";

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerNum = sm.SQLDB.get_Fields("DealerNo");
            }
            return dealerNum;
        }

        public string GetDealerNamebyDealerNo(string dealerNo)
        {
            string dealerName = String.Empty;
            SQL_Master sm = new();
            sm.MasterSql = "Select DealerName from Dealer where DealerNo =  '" + dealerNo + "'";
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                dealerName = sm.SQLDB.get_Fields("DealerName");
            }
            return dealerName;
        }
        #endregion Dealers

        #region Agents
        public int? GetAgentIDbyAgentName(string agentName)
        {
            if (string.IsNullOrEmpty(agentName))
            {
                return null;
            }

            int agentID = 0;
            agentName = CleanString(agentName, false);

            List<string> agentNames = new();
            agentNames = Agent.GetListOfCommonSpellingsOfAgentNames(agentName);

            SQL_Master sm = new();
            foreach (string name in agentNames)
            {
                sm.MasterSql = "Select AgentID from Agents where AgentName = '" + name + "'";
                if (sm.OpenDB(true))
                {
                    sm.SQLDB.GetRow();
                    string sAgentID = sm.SQLDB.get_Fields("AgentID");
                    bool containsData = !string.IsNullOrEmpty(sAgentID);
                    if (containsData)
                    {
                        bool success = Int32.TryParse(sAgentID, out agentID);
                        if (success)
                        {
                            return agentID;
                        }
                    }
                }
                else
                {
                    continue;
                }
            }

            return null;
        }

        public string GetAgentNoFromAgentID(int agentID)
        {
            string agentNo = null;
            SQL_Master sm = new();
            MasterSql = "Select AgentNO from Agents where AgentID = " + agentID;

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                agentNo = sm.SQLDB.get_Fields("AgentNO").ToString();

            }
            return agentNo;
        }

        public int? GetAgentIDbyContactName(string contact)
        {
            int agentID = 0;
            contact = CleanString(contact, false);

            SQL_Master sm = new();

            sm.MasterSql = "Select AgentID from Agents where Contact = '" + contact + "'";
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                string sAgentID = sm.SQLDB.get_Fields("AgentID");
                bool containsData = !string.IsNullOrEmpty(sAgentID);
                if (containsData)
                {
                    bool success = Int32.TryParse(sAgentID, out agentID);
                    if (success)
                    {
                        return agentID;
                    }
                }

                Globals.BadFileMessage += Globals.AGENTID__NOTFOUND_BY_CONTACT + contact;
            }
            return null;
        }
        public int GetAgentIDFromContractID(int? contractID)
        {
            int agentID = 0;
            SQL_Master sm = new();
            sm.MasterSql = "Select AgentsID from Contract where ContractID =  " + contractID;

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                agentID = int.Parse(sm.SQLDB.get_Fields("AgentsID"));
            }
            return agentID;
        }

        public int GetAgentIDFromDealerID(int? dealerID)
        {
            if (!dealerID.HasValue)
            {
                return 0;
            }

            int agentID = 0;
            SQL_Master sm = new();
            sm.MasterSql = "Select AgentsID from Dealer where DealerID =  " + dealerID;

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                agentID = int.Parse(sm.SQLDB.get_Fields("AgentsID"));
            }
            return agentID;
        }

        public string GetAgentNameByAgentID(int? agentID)
        {
            string agentName = null;
            SQL_Master sm = new();
            sm.MasterSql = "Select AgentName from Agents where AgentID = " + agentID;

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                agentName = sm.SQLDB.get_Fields("AgentName").ToString();
                Globals.AgentName = agentName;
            }
            return agentName;
        }
        #endregion Agents

        #region Contracts
        public int? GetContractID(string contractNo)
        {
            int? contractID = null;
            contractNo = CleanString(contractNo);
            SQL_Master sm = new();          
            sm.MasterSql = "Select ContractID from Contract where ContractNo =  '" + contractNo + "'";
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                contractID = int.Parse(sm.SQLDB.get_Fields("ContractID"));
            }
            return contractID;
        }
        public DateTime? GetEffCancelDateFromContractID(int? contractID)
        {
            if (contractID == null)
            {
                return null;
            }
            DateTime? effCancelDate = null;
            SQL_Master sm = new();
            sm.MasterSql = "Select CancelEffDate from ContractCancel where ContractID =  " + contractID;
            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                effCancelDate = DateTime.Parse(sm.SQLDB.get_Fields("CancelEffDate"));
            }
            return effCancelDate;
        }
        public DateTime? GetPaidDateFromContract(int? contractID)
        {
            if (contractID == null)
            {
                return null;
            }
            DateTime? paidDate = null;
            SQL_Master sm = new();
            sm.MasterSql = "Select DatePaid from Contract where ContractID =  " + contractID;

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                string paid = sm.SQLDB.get_Fields("DatePaid").ToString();
                if (string.IsNullOrEmpty(paid))
                {
                    return null;
                }
                try
                {
                    paidDate = DateTime.Parse(paid);
                }
                catch (Exception ex)
                {
                    Globals.IsBadFile = true;
                    Globals.BadFileMessage = "Error: SQL = " + sm.MasterSql + " : Exception = " + ex.Message;
                    return null;
                }
            }
            return paidDate;
        }
        #endregion Contracts

        public DateTime? GetSaleDateFromContract(int contractID)
        {
            if (contractID == 0)
            {
                return null;
            }
            DateTime? paidDate = null;
            SQL_Master sm = new();
            sm.MasterSql = "Select SaleDate from Contract where ContractID =  " + contractID;

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                string saleDate = sm.SQLDB.get_Fields("SaleDate").ToString();
                if (string.IsNullOrEmpty(saleDate))
                {
                    return null;
                }
                try
                {
                    paidDate = DateTime.Parse(saleDate);
                }
                catch (Exception ex)
                {
                    Globals.IsBadFile = true;
                    Globals.BadFileMessage = "Error: SQL = " + sm.MasterSql + " : Exception = " + ex.Message;
                    return null;
                }
            }
            return paidDate;
        }

        public decimal GetDealerCostFromContract(int? contractID)
        {
            if (!contractID.HasValue || contractID == 0)
            {
                return -1;
            }
            decimal dDealerCost = -1;
            SQL_Master sm = new();
            sm.MasterSql = "Select DealerCost from Contract where ContractID =  " + contractID;

            if (sm.OpenDB(true))
            {
                sm.SQLDB.GetRow();
                string dealerCost = sm.SQLDB.get_Fields("DealerCost").ToString();
                if (string.IsNullOrEmpty(dealerCost))
                {
                    return -1;
                }
                try
                {
                    dDealerCost = Decimal.Parse(dealerCost);
                }
                catch (Exception ex)
                {
                    Globals.IsBadFile = true;
                    Globals.BadFileMessage = "Error: SQL = " + sm.MasterSql + " : Exception = " + ex.Message;
                    return -1;
                }
            }
            return dDealerCost;
        }
    }
}
