﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommisionImport.SQL;

namespace CommisionImport
{
    public static class Agent
    {
        public static string FixSpellingOfAgent(string agentName)
        {
            if (string.IsNullOrEmpty(agentName))
            {
                return null;
            }

            agentName = fixAgentName(agentName).Trim();
            string upperAgentName = agentName.ToUpper();
            if (upperAgentName.Contains("KOONS"))
            {
                agentName = "Dealer First Automotive LLC";
            }

            if (upperAgentName == "CBS MARKETING" || upperAgentName == "CLG AUTOMOTIVE" || upperAgentName == "DEALER FIRST AUTOMOTIVE" ||
                upperAgentName == "GULFSTREAM PARTNERS" || upperAgentName == "VERO" || upperAgentName == "CBS MARKETING GROUP")
            {
                agentName = agentName + " LLC";
            }

            else if (agentName.StartsWith("T.A.S") | agentName == "TASA of KY Inc")
            {
                agentName = "T.A.S.A. of KY, INC";
            }
            else if (agentName.Contains("ADS, Inc"))
            {
                agentName = "ADS Inc";
            }
            else if (upperAgentName.Contains("ROBERT CORBERTT"))
            {
                agentName = "Bob Corbett";
            }
            else if (upperAgentName.Contains("LORYBO"))
            {
                agentName = "LoRyBo";
            }
            else if (upperAgentName.Contains("MID SOUTH"))
            {
                agentName = agentName.Replace("BARRY BUCKLEY - ", "");
                agentName = agentName.Replace("MID SOUTH", "Mid-South");
                agentName = agentName.Trim() + " INC";
            }
            else if (upperAgentName == "CBS MARKETING" || upperAgentName == "CLG AUTOMOTIVE" ||
                upperAgentName == "GULFSTREAM PARTNERS" || upperAgentName == "VERO" || upperAgentName == "CBS MARKETING GROUP")
            {
                agentName = agentName.Trim() + " LLC";
            }
            else if (agentName.Contains("Cobus  Automotive"))
            {
                agentName = agentName.Replace("Cobus  Automotive", "Cobus Automotive").Trim();
                agentName += " -Chris Spitaleri";
            }
            else if (upperAgentName == "PAUL  PATTERSON")
            {
                agentName = "Paul Patterson";
            }
            else if (agentName.Contains("Henry Stumpf(FWG)"))
            {
                agentName = agentName.Replace(" Henry Stumpf(FWG)", "");
            }
            else if (upperAgentName.Contains("FLEET WARRANTY"))
            {
                agentName = "Fleet Warranty Group";
            }
            else if (upperAgentName.Contains("COAST  AUTOMOTIVE"))
            {
                agentName = "Gulf Coast Automotive Warranty LLC";
            }
            else if (upperAgentName == "INSURED DEALER PROFITS")
            {
                agentName = "Insured Dealer Profits, LLC";
            }
            else if (upperAgentName == "IMOXY")
            {
                agentName = "Moxy";
            }
            else if (upperAgentName == "JK PRADO INC ( HORIZON EQUITY GROUP )")
            {
                agentName = "Horizon Equity Group";
            }
            else if (agentName == "Milestone Dealer Services")
            {
                agentName = "Milestone Dealer Solutions LLC";
            }
            else if (upperAgentName == "PEAK PERFORMANCE TEAM")
            {
                agentName = "Peak Performance Team Inc";
            }
            else if (upperAgentName == "PEAK  PERFORMANCE TEAM")
            {
                agentName = "Peak Performance Team Inc";
            }
            else if (upperAgentName == "TN DEALER CONSULTANTS")
            {
                agentName = "TN Dealer Consultants, LLC";
            }
            else if (upperAgentName == "AUTONATION")
            {
                agentName = "Auto Nation Corporate";
            }

            return agentName;
        }

        public static List<string> GetListOfCommonSpellingsOfAgentNames(string agentName)
        {
            agentName = fixAgentName(agentName);
            string upperAgentName = agentName.ToUpper();
            List<string> agentNames = new();
            string newAgentName = string.Empty;

            agentNames.Add(agentName);

            if (upperAgentName == "AUTONATION")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Auto Nation Corporate";
            }
            else if (upperAgentName.Contains("CIRILLO"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                agentName = "Rich Cirillo";
            }
            else if (agentName.StartsWith("T.A.S") || agentName.StartsWith("TASA"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "T.A.S.A. of KY, INC";
            }
            else if (agentName.Contains("ADS, INC"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "ADS Inc";
            }
            else if (agentName.Contains("Robert Corb"))
            {
                newAgentName = "Bob Corbett";
                agentNames.Add("Robert Corbett");
                agentNames.Add("Robert Corbet");
                agentNames.Add("Bob Corbet");
            }
            else if (upperAgentName.Contains("LORYBO"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "LoRyBo";
            }
            else if (upperAgentName.Contains("MID SOUTH"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Mid-South Dealer Services INC";
            }
            else if (upperAgentName.Contains("COBUS  AUTOMOTIVE"))
            {
                newAgentName = agentName.Replace("Cobus  Automotive", "Cobus Automotive").Trim();
                newAgentName += " -Chris Spitaleri";
            }
            else if (agentName.Contains("Paul  Patterson"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = agentName.Replace("Paul  Patterson", "Paul Patterson");
            }
            else if (agentName.Contains("Henry Stumpf(FWG)"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = agentName.Replace(" Henry Stumpf(FWG)", "");
            }
            else if (agentName.Contains("Fleet Warranty"))
            {
                if (!agentName.Contains("Group"))
                {
                    newAgentName = agentName.Trim() + " Group";
                }
            }
            else if (upperAgentName.Contains("Coast  Automotive"))
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Gulf Coast Automotive Warranty LLC";
            }
            else if (agentName.ToUpper() == "INSURED DEALER PROFITS")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Insured Dealer Profits, LLC";
            }
            else if (agentName.ToUpper() == "IMOXY")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Moxy";
            }
            else if (upperAgentName == "JK PRADO INC ( HORIZON EQUITY GROUP )")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Horizon Equity Group";
            }
            else if (upperAgentName == "MILESTONE DEALER SERVICES")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Milestone Dealer Solutions LLC";
            }
            else if (upperAgentName == "PEAK PERFORMANCE TEAM" || upperAgentName == "PEAK  PERFORMANCE TEAM")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "Peak Performance Team Inc";
            }
            else if (upperAgentName == "TN DEALER CONSULTANTS")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                newAgentName = "TN Dealer Consultants, LLC";
            }
            else if (upperAgentName == "DEALERS CHOICE")
            {
                agentNames = removeAgentFromList(agentNames, agentName);
                agentName = "Dealers Choice Inc";
            }


            if (!agentNames.Contains(newAgentName) && !string.IsNullOrEmpty(newAgentName))
            {
                agentNames.Add(newAgentName);
            }
            return agentNames;
        }

        private static List<string> removeAgentFromList(List<string> agentNames, string agentName)
        {
            List<int> newListIndexes = new();
            for (int i = 0; i < agentNames.Count; i++)
            {
                if (agentNames[i].ToUpper() == agentName.ToUpper())
                {
                    newListIndexes.Add(i);
                }
            }

            for (int i = 0; i < newListIndexes.Count; i++)
            {
                agentNames.RemoveAt(i);
            }
            return agentNames;
        }

        private static string fixAgentName(string agentName)
        {
            if (string.IsNullOrEmpty(agentName))
            {
                return null;
            }
            foreach (string value in Globals.ValuesToRemoveFromAgentName)
            {
                if (agentName.Contains(value))
                {
                    agentName = agentName.Replace(value, "");
                    break;
                }
            }

            return agentName;
        }

        public static int? GetAgentIDByAgentName(PublicClasses.Objects_XL obj, bool fixSpelling = true)
        {
            if (string.IsNullOrEmpty(obj.AgentName))
            {
                return 0;
            }

            string agentName = string.Empty;
            int? agentID = 0;

            if (!string.IsNullOrEmpty(obj.AgentName))
            {
                agentName = obj.AgentName.ToUpper();
                if (fixSpelling)
                {
                    agentName = FixSpellingOfAgent(agentName);
                }
                agentID = GetAgentID(agentName);
                if (agentID > 0)
                {
                    return agentID;
                }
            }
            SQL_Select ss = new();

            agentID = ss.GetAgentIDbyAgentName(agentName);
            if (agentID > 0)
            {
                return agentID;
            }
            else
            {
                agentID = ss.GetAgentIDbyContactName(agentName);
                if (agentID == 0)
                {
                    agentID = ss.GetAgentIDFromContractID(obj.ContractID);
                    if (agentID == 0)
                    {
                        agentID = ss.GetAgentIDFromDealerID(obj.DealerID);
                        if (agentID == 0)
                        {
                            Globals.BadFileMessage = Globals.AGENTID_NOTFOUND_BY_NAME + agentName + ", nor DealerID = " + obj.DealerID;
                            Globals.IsBadFile = true;
                            return 0;
                        }
                    }
                }
            }
            return agentID;
        }

        public static int? GetMGA_AgentIDByAgentName(PublicClasses.Objects_XL obj, bool fixSpelling = true)
        {
            if (string.IsNullOrEmpty(obj.AgentName))
            {
                return null;
            }

            string upAgentName = obj.AgentName.ToUpper();
            int? agentID = 0;
            if (fixSpelling)
            {
                FixSpellingOfAgent(upAgentName);
            }

            agentID = GetAgentID(upAgentName);
            if (agentID > 0 )
            {
                return agentID;
            }
            SQL_Select ss = new();

            agentID = ss.GetAgentIDbyAgentName(upAgentName);
            if (agentID > 0)
            {
                return agentID;
            }
            else
            {
                agentID = ss.GetAgentIDbyContactName(upAgentName);
                if (agentID == 0)
                {
                    agentID = ss.GetAgentIDFromContractID(obj.ContractID);
                    if (agentID == 0)
                    {
                        agentID = ss.GetAgentIDFromDealerID(obj.DealerID);
                        if (agentID == 0)
                        {
                            Globals.BadFileMessage = Globals.AGENTID_NOTFOUND_BY_NAME + upAgentName + ", nor ContractID = " + obj.ContractID + ", nor DealerID = " + obj.DealerID;
                            Globals.IsBadFile = true;
                            return 0;
                        }
                    }
                }
            }
            return agentID;
        }

        public static int? GetAgentID(string agentName)
        {            
            if (string.IsNullOrEmpty(agentName))
            {
                return 0;
            }

            string upAgentName = agentName.ToUpper();
            if (string.IsNullOrEmpty(agentName))
            {
                return 0;
            }

            if (upAgentName.Contains("BERMAN"))
            {
                return 1;
            }
            else if (upAgentName.Contains("CORBE"))
            {
                return 1252;
            }
            else if (upAgentName.Contains("DAWN"))
            {
                return 1463;
            }
            else if (upAgentName.Contains("DEALER") && (upAgentName.Contains("FIRST")))
            {
                return 1463;
            }
            else if (upAgentName.Contains("NORTH COUNTY ENTERPRISES"))
            {
                return 1099;
            }
            else if (upAgentName.Contains("I-90"))
            {
                return 1532;
            }
            else if (upAgentName.Contains("WARRANTY SOL 2"))
            {
                return 1504;
            }
            else if (upAgentName.Contains("MILESTONE") && (upAgentName.Contains("DEALER")))
            {
                return 1060;
            }
            else if (upAgentName.Contains("FAROOQ"))
            {
                return 1268;
            }
            else if (upAgentName.Contains("CARTEL"))
            {
                return 1528;
            }
            else if (upAgentName.Contains("DAR "))
            {
                return 26;
            }
            else if (upAgentName.Contains("GUTSCHOW"))
            {
                return 1325;
            }
            else if (upAgentName.Contains("MID-SOUTH"))
            {
                return 1065;
            }
            else if (upAgentName.Contains("PACIFIC") && upAgentName.Contains("DEALER"))
            {
                return 1511;
            }
            else if (upAgentName.Contains("F&F") || upAgentName.Contains("F & F"))
            {
                return 1505;
            }
            else if (upAgentName.Contains("BURGHOLZER"))
            {
                return 1541;
            }
            else if (upAgentName.Contains("LEVEL 5"))
            {
                return 1217;
            }
            else if (upAgentName.Contains("RPM"))
            {
                return 1565;
            }
            else if (upAgentName.Contains("COBUS AUTOMOTIVE"))
            {
                return 18;
            }
            else if (upAgentName.Contains("AUTO DEALER ONE"))
            {
                return 1134;
            }
            else if (upAgentName.Contains("AUTO DEALER ONE"))
            {
                return 1134;
            }
            else if (upAgentName.Contains("DEALER") && upAgentName.Contains("INDIANA"))
            {
                return 1111;
            }
            else if (upAgentName.Contains("SOUTH") && upAgentName.Contains("COAST")
                && upAgentName.ToUpper().Contains("AGENCY"))
            {
                return 1302;
            }
            else if (upAgentName.ToUpper().Contains("PEAK") && upAgentName.ToUpper().Contains("PERFORMANCE")
               && upAgentName.ToUpper().Contains("CONSULTING"))
            {
                return 1265;
            }
            else if (upAgentName.Contains("PEAK") && upAgentName.Contains("PERFORMANCE")
                && upAgentName.ToUpper().Contains("TEAM"))
            {
                return 1533;
            }
            else if (upAgentName.Contains("WILGUS"))
            {               
                return 1180;
            }
            if (upAgentName.Contains("CAR CONCIERGE SERVICES"))
            {
                return 1347;
            }
            if (upAgentName.Contains("PRITCHARD"))
            {
                return 1135;
            }
            if (upAgentName.Contains("ACE GROUP"))
            {
                return 1125;
            }
            if (upAgentName.Contains("ARNOLD"))
            {
                return 1570;
            }
            if (upAgentName.Contains("COVENANT DEALER"))
            {
                return 1140;
            }  
            else if (upAgentName.ToUpper().Contains("SHOPSHIRE"))
            {
                return 1529;
            }
            else if (upAgentName.ToUpper().Contains("KEN NORTH"))
            {
                return 1168;
            }
            else if (upAgentName.ToUpper().Contains("COUGHLIN"))
            {
                return 54;
            }
            else if (upAgentName.ToUpper().Contains("CLAUSE"))
            {
                return 54;
            }

            return 0;
        }

        public static int? GetSheetAgentIDByAgentName(PublicClasses.Objects_XL obj, bool fixSpelling = true)
        {
            string sheetAgentName = obj.SheetAgent;

            if (string.IsNullOrEmpty(sheetAgentName))
            {
                return null;
            }

            if (fixSpelling)
            {
                FixSpellingOfAgent(sheetAgentName);
            }

            int? agentID = GetAgentID(sheetAgentName);
            if (agentID > 0)
            {
                return agentID;
            }           

            SQL_Select ss = new();

            agentID = ss.GetAgentIDbyAgentName(sheetAgentName);
            if (agentID > 0)
            {
                return agentID;
            }
            else
            {
                agentID = ss.GetAgentIDbyContactName(sheetAgentName);
            }
            return agentID;
        }

        public static string GetAgentNameByAgentID(PublicClasses.Objects_XL obj)
        {
            SQL_Select ss = new();
            if (obj.AgentID == null)
            {
                return null;   
            }
            string agentName = string.Empty;
      
            agentName = ss.GetAgentNameByAgentID(obj.AgentID);

            if (string.IsNullOrEmpty(agentName))
            {
                StaticFuncts.WriteLine(Globals.AGENTNAME_NOTFOUND_BY_AGENTID + obj.AgentID);
            }
            return agentName;
        }

        public static int? GetAgentIDByContractID(PublicClasses.Objects_XL obj)
        {
            if (!obj.ContractID.HasValue)
            {
                Globals.IsBadFile = true;
                return 0;
            }
            int? agentID = GetAgentIDByAllMeans(obj);

            return agentID;
        }

        public static int? GetAgentIDByAllMeans(PublicClasses.Objects_XL obj)
        {
            SQL_Select ss = new();
            int? agentID = GetAgentID(obj.AgentName);
            if (agentID > 0)
            {
                return agentID;
            }

            agentID = ss.GetAgentIDFromContractID(obj.ContractID);
            if (agentID == 0)
            {
                agentID = ss.GetAgentIDFromDealerID(obj.DealerID);
                if (agentID == 0)
                {
                    agentID = ss.GetAgentIDbyContactName(obj.AgentName);
                    if (agentID == 0)
                    {
                        Globals.BadFileMessage = Globals.AGENTID_NOTFOUND_BY_CONTRACTNO + obj.ContractID + ", nor DealerID = " + obj.DealerID;
                        Console.WriteLine(Globals.BadFileMessage);
                        Globals.IsBadFile = true;
                    }
                }
                
            }
            return agentID;
        }
    }
}
