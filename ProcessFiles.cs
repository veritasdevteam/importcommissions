﻿using System.Collections.Generic;
using System.Data;
using CommisionImport.SQL;
using System;
using System.IO;
using System.Text;

namespace CommisionImport
{
    static public class ProcessFiles
    {
        public static bool ProcessXLFiles()
        {
            Globals.Initialize();
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            SQL_Insert sql = new();

            List<string> files = StaticFuncts.GetFiles();
            if (files.Count == 0)
            {
                //No Files To Process
                return false;
            }
            Console.WriteLine("Found Files: " + files.Count);

            foreach (string file in files)
            {
                SQL_Select ss = new();

                Globals.FileNameProcessing = Path.GetFileName(file);
                StaticFuncts.WriteLine("Processing: " + Globals.FileNameProcessing);
                if (ss.IsDupeRecord(Globals.FileNameProcessing))
                {
                    continue;
                }

                /// else...
                Globals.FilePathProcesing = file;

                string fileType = StaticFuncts.GetFileType(Globals.FileNameProcessing);
                if (Globals.IsDateInFileName)
                {
                    StaticFuncts.SetDateFromFileName(Globals.FileNameProcessing);
                }
                List<PublicClasses.Objects_XL> ClassListToInsert = new();
                DataSet ds = StaticFuncts.ConvertExcelToDataSet(file);
                if (ds == null)
                {
                    continue;
                }

                int columnCount = ds.Tables[0].Columns.Count;

                string columnsAppConfig = columnCount + "_TotalFieldColumn";
                Globals.SetColumnTotalField(columnsAppConfig);
                PublicClasses.BooleanValues bv = new();
                PublicClasses.ColumnValues cols = new();
                foreach (DataTable tb in ds.Tables)
                {
                    Globals.AgentName = string.Empty;
                    bv.HasMgaColumn = StaticFuncts.DoesSheetHaveColumn(tb, "MGA");
                    bv.HasAgentColumn = StaticFuncts.DoesSheetHaveColumn(tb, "AGENT");
                    bv.HasProductTypeColumn = StaticFuncts.DoesSheetHaveColumn(tb, "PRODUCT TYPE");
                    cols = PublicClasses.GetColumnValues(columnCount, bv);

                    ClassListToInsert = ProcessFiles.ConvertDataTableIntoListClass(cols, tb, fileType, Globals.FileNameProcessing, bv);
                    if (ClassListToInsert.Count > 0)
                    {
                        bool itWorked = sql.InsertIntoTable_ObjectList(ClassListToInsert);
                        if (!itWorked)
                        {
                            Globals.IsBadFile = true;
                            StaticFuncts.WriteLine("ERROR: " + Globals.BadFileMessage);
                            StaticFuncts.WriteErrorLog();
                        }
                        //StaticFuncts.WriteClassListToXMLFile(ClassListToInsert);
                    }
                    else
                    {
                        Globals.IsBadFile = true;
                        StaticFuncts.WriteLine("ERROR: " + Globals.BadFileMessage);
                        StaticFuncts.WriteErrorLog();
                    }

                    try
                    {
                        ds.Dispose();
                        StaticFuncts.MoveFile();
                        Globals.IsBadFile = false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return true;
        }

        public static List<PublicClasses.Objects_XL> ConvertDataTableIntoListClass(PublicClasses.ColumnValues cols, DataTable dt, string fileType, string fileName, PublicClasses.BooleanValues bv)
        {
            List<PublicClasses.Objects_XL> list = new();
            SQL_Select ss = new();
            bool isCancellation = false;
            bool skipRow = false;
            bool isColZeroDealerName = false;
            int counter = 0;
            int? agentID = null;
            List<string> columnNames = new();
            string sheetAgent = string.Empty;
            int? sheetID = 0;
            bool isFlorida = false;
            isFlorida = StaticFuncts.SetIsFlorida();
            bool isOverfund = false;
            int programTypeColNum = -1;
            if (bv.HasProductTypeColumn)
            {
                programTypeColNum = StaticFuncts.GetColumnNumber(dt, "PRODUCT TYPE");
            }

            foreach (DataRow ro in dt.Rows)
            {
                PublicClasses.Objects_XL obj = new();
                obj.IsValid = true;
                if (counter == 0)
                {
                    sheetAgent = ro[0].ToString().Trim();
                    sheetAgent = Agent.FixSpellingOfAgent(sheetAgent);
                    obj.SheetAgent = sheetAgent;
                    sheetID = Agent.GetSheetAgentIDByAgentName(obj, false);
                    obj.SheetAgentID = sheetID;
                }
                else
                {
                    obj.SheetAgent = sheetAgent;
                    obj.SheetAgentID = sheetID;
                }

                counter++;

                StaticFuncts.WriteLine("Row Processing = " + counter);

                string col1Value = ro[1].ToString();
                string colZeroValue = ro[0].ToString();
                if (colZeroValue.ToUpper().Contains("DEDUCTION") || col1Value.ToUpper().Contains("DEDUCTION"))
                {
                    Globals.IsClaimDeductionFile = true;
                    return list;
                }

                if (colZeroValue.ToUpper().Contains("APPROVED") || col1Value.ToUpper().Contains("APPROVED"))
                {                    
                    // Per Charlee: Do Nothing.
                    return list;
                }

                if (colZeroValue.ToUpper().Contains("OVERFUND") || col1Value.ToUpper().Contains("OVERFUND"))
                {
                    isOverfund = true;
                    isCancellation = false;
                    skipRow = true;
                    continue;
                }

                colZeroValue = ro[0].ToString().ToUpper();
                if (colZeroValue == "DEALER NAME")
                {
                    cols = PublicClasses.SetRowZeroDealerNameColumnIndexes();
                }

                string agentNameTemp = ro[0].ToString().Trim();
                if (string.IsNullOrEmpty(obj.SheetAgent))
                {
                    obj.SheetAgent = Agent.FixSpellingOfAgent(agentNameTemp);
                    if (!sheetAgent.Contains("YIELD SOLUTIONS"))
                    {
                        obj.SheetAgentID = Agent.GetSheetAgentIDByAgentName(obj);
                    }
                }
                if (counter >= Globals.StartRow)
                {
                    string colZero = ro[0].ToString().Trim().ToUpper();
                    string colOne = ro[1].ToString().Trim().ToUpper();

                    //bool containsCancelColumn = StaticFuncts.DoesRowHaveColumn(ro, "Cancel %");
                    if (colZero.Contains("CANCELLATION") || colOne.Contains("CANCELLATION"))
                    {
                        isCancellation = true;
                        isOverfund = false;
                        //if (!containsCancelColumn)
                        //{
                            skipRow = true;
                        //}
                        continue;
                    }

                    if (colZero.ToUpper().Contains("SPIFF"))
                    {
                        skipRow = true;
                        isOverfund = false;
                        continue;
                    }

                    if (colZero.ToUpper().Contains("PRODUCTS"))
                    {
                        skipRow = true;
                        continue;
                    }

                    if (colZero.ToUpper().Contains("OVERFUND") || colOne.ToUpper().Contains("OVERFUND"))
                    {
                        isOverfund = true;
                        skipRow = true;
                        continue;
                    }

                    if (bv.HasMgaColumn)
                    {
                        string mga = ro[cols.MGA_Col].ToString().Trim();
                        if (!string.IsNullOrEmpty(mga))
                        {
                            obj.MGA = Agent.FixSpellingOfAgent(mga);
                            obj.MGA_ID = Agent.GetMGA_AgentIDByAgentName(obj);
                        }
                    }
                    if (bv.HasAgentColumn)
                    {
                        string agentName = ro[cols.AgentName_Col].ToString().Trim();
                        agentName = Agent.FixSpellingOfAgent(agentName);
                        obj.AgentName = agentName;
                        obj.AgentID = Agent.GetAgentIDByAgentName(obj);
                    }


                    if (skipRow)
                    {
                        columnNames = StaticFuncts.GetColumnNames(ro);
                        if (columnNames.Contains("CANCEL DATE") || columnNames.Contains("Eff Date"))
                        {
                            skipRow = false;
                        }
                        skipRow = false;
                        continue;
                    }

                    if (StaticFuncts.IsEmptyRow(ro))
                    {
                        continue;
                    }


                    if (ss.IsDupeRecord(fileName.Trim()))
                    {
                        Console.WriteLine("Duplicate files found: " + fileName);
                        continue;
                    }
                    if (bv.HasProductTypeColumn && !isCancellation)
                    {
                        obj.ProductType = ro[programTypeColNum].ToString();
                    }

                    obj.FileName = fileName;
                    obj.FileType = fileType;
                    obj.CycleDate = Globals.CycleDate;
                    obj.IsCancelled = isCancellation;
                    obj.IsOverfund = isOverfund;

                    obj.ContractNo = ro[cols.ContractNo_Col].ToString();
                    obj.ContractID = StaticFuncts.GetContractID(obj.ContractNo);
                    if (bv.HasMgaColumn)
                    {
                        string mgaName = ro[cols.MGA_Col].ToString();
                        obj.MGA = Agent.FixSpellingOfAgent(mgaName);
                        obj.MGA_ID = Agent.GetMGA_AgentIDByAgentName(obj);
                    }
                    if (bv.HasAgentColumn)
                    {
                        string name = ro[cols.AgentName_Col].ToString();
                        if (!string.IsNullOrEmpty(name))
                        {                            
                            obj.AgentName = name;
                            obj.AgentID = Agent.GetAgentIDByAgentName(obj);
                        }
                    }

                    // Contract
                    if (!obj.AgentID.HasValue || obj.AgentID == 0)
                    {
                        int? newAgentID = Agent.GetAgentIDByAllMeans(obj);
                        if (newAgentID > 0)
                        {
                            obj.AgentID = newAgentID;
                        }
                        else
                        {
                            if (Globals.IsBadFile)
                            {
                                list.Clear();
                                return list;
                            }

                            string tempAgentName = Agent.GetAgentNameByAgentID(obj);
                            if (string.IsNullOrEmpty(tempAgentName) && !obj.AgentID.HasValue)
                            {
                                if (!string.IsNullOrEmpty(obj.AgentName))
                                {
                                    obj.AgentName = obj.AgentName.Replace("  ", " ");
                                    obj.AgentID = ss.GetAgentIDbyAgentName(obj.AgentName);
                                }
                            }
                        }
                    }

                    // Dealer
                    if (isColZeroDealerName)
                    {
                        obj.DealerName = ro[cols.DealerName_Col].ToString();

                        obj.DealerID = Dealer.GetDealerIDWithDealerName(obj.DealerName);
                        if (!obj.DealerID.HasValue)
                        {
                            obj.DealerID = Dealer.GetDealerIDByContractID(obj.ContractID);
                        }
                        obj.DealerNo = Dealer.GetDealerNoWithDealerID(obj.DealerID);
                    }
                    else
                    {
                        obj = Dealer.GetDealerInfo(ro, obj, cols);

                        if (!isColZeroDealerName)
                        {
                            if (string.IsNullOrEmpty(obj.SheetAgent))
                            {
                                obj = setAgentData(obj, ro, cols);
                            }

                            else
                            {
                                if (!agentID.HasValue || agentID == null)
                                {
                                    obj.SheetAgent = Agent.FixSpellingOfAgent(obj.SheetAgent);
                                    agentID = Agent.GetAgentIDByAllMeans(obj);
                                    if (Globals.IsBadFile)
                                    {
                                        list.Clear();
                                        return list;
                                    }
                                    if (bv.HasAgentColumn)
                                    {
                                        obj.AgentName = ro[cols.AgentName_Col].ToString();
                                    }
                                    else
                                    {
                                        Globals.AgentName = obj.AgentName;
                                    }
                                }
                            }
                        }
                        else
                        {
                            agentID = Agent.GetAgentIDByContractID(obj);
                            obj.AgentID = agentID;
                            if (agentID == 0 || agentID == null)
                            {
                                int? newAgentID = Agent.GetAgentIDByAllMeans(obj);
                                if (Globals.IsBadFile)
                                {
                                    list.Clear();
                                    return list;
                                }
                                else
                                {
                                    obj.AgentID = newAgentID;
                                }
                            }
                            obj.AgentName = Agent.GetAgentNameByAgentID(obj);
                        }

                        // Customer
                        obj.CustName = ro[cols.CustName_Col].ToString();

                        // Sale Date
                        obj.SaleDate = StaticFuncts.getDateTime(ro[cols.SaleDate_Col]);
                        obj.Amt = StaticFuncts.parseDecimal(ro[cols.Amt_Col].ToString());
                        // Costs, Dates, Percents
                        if (!isCancellation) // Not Cancelled
                        {
                            obj.FundingAmt = StaticFuncts.parseDecimal(ro[cols.FundingAmt_Col]);
                            obj.FundedDate = StaticFuncts.getDateTime(ro[cols.FundedDate_Col]);
                            obj.Amt = StaticFuncts.parseDecimal(ro[cols.Amt_Col].ToString());
                        }
                        else // Is Cancelled
                        {
                            if (cols.CancelledFundedDate_Col > -1)
                            {
                                obj.FundedDate = StaticFuncts.GetPaidDateForContractID(obj.ContractID);
                            }

                            if (cols.DealerCost_Col > -1)
                            {
                                obj.DealerCost = StaticFuncts.parseDecimal(ro[cols.DealerCost_Col]);
                            }
                            else
                            {
                                obj.DealerCost = ss.GetDealerCostFromContract(obj.ContractID);
                                if (obj.DealerCost == -1)
                                {
                                    Globals.IsBadFile = true;
                                    Globals.BadFileMessage = "NO Dealer Cost found from: " + obj.ContractID;
                                    list.Clear();
                                    return list;
                                }
                            }

                            obj.CancelPercent = StaticFuncts.parseDecimal(ro[cols.CancelPercent_Col]);

                            if (cols.EffCancelDate_Col > -1)
                            {
                                obj.EffCancelDate = StaticFuncts.getDateTime(ro[cols.EffCancelDate_Col]);
                            }
                            else
                            {
                                obj.EffCancelDate = StaticFuncts.GetEffCancelDateFromContractID(obj.ContractID);
                            }

                            if (cols.DaysActive_Col > -1)
                            {
                                string daysActiveValue = ro[cols.DaysActive_Col].ToString().Trim();
                                if (daysActiveValue.ToUpper().Contains("NO ANCB"))
                                {
                                    obj.IsANCB = true;
                                    var dateDiff = obj.EffCancelDate - obj.SaleDate;
                                    obj.DaysActive = dateDiff.GetValueOrDefault().Days;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(daysActiveValue))
                                    {
                                        obj.DaysActive = int.Parse(daysActiveValue);
                                    }
                                }
                            }
                            else
                            {
                                var dateDiff = obj.EffCancelDate - obj.SaleDate;
                                obj.DaysActive = dateDiff.GetValueOrDefault().Days;
                            }

                            obj.IsFlorida = isFlorida;
                            obj = StaticFuncts.SetDateDataForRowItem(obj);
                            obj = StaticFuncts.FixAgentOrDealerNulls(obj, cols, bv, ro, true);
                        }

                        if (Globals.IsBadFile)
                        {
                            list.Clear();
                            return list;
                        }

                        obj = StaticFuncts.FixAgentOrDealerNulls(obj, cols, bv, ro);
                        if (Globals.IsBadFile)
                        {
                            Globals.IsBadFile = true;
                            List<string> agents = new();
                            agents.Add(obj.AgentName);
                            agents.Add(obj.MGA);
                            Globals.BadFileMessage = string.Format(Globals.AGENTID_NOTFOUND_BY_MGA_NOR_AGENT_TEMPLATE, agents.ToArray());
                            list.Clear();
                            return list;
                        }

                        if (!StaticFuncts.ContainsNulls(obj))
                        {
                            list.Add(obj);
                        }
                    }
                }
            }
            return list;
        }

        private static PublicClasses.Objects_XL setAgentData(PublicClasses.Objects_XL obj, DataRow ro, PublicClasses.ColumnValues cols)
        {
            if (string.IsNullOrEmpty(Globals.AgentName))
            {
                string agentName = ro[cols.AgentName_Col].ToString();
                Globals.AgentName = agentName;
                obj.AgentName = agentName;
                int? agentID = Agent.GetAgentIDByAgentName(obj);
                if (agentID == 0)
                {
                    agentID = Agent.GetAgentIDByContractID(obj);
                    Globals.IsBadFile = true;
                    return obj;
                }
                else
                {
                    agentID = Agent.GetAgentIDByAgentName(obj);
                    obj.AgentID = agentID;
                    if (agentID == 0)
                    {
                        agentName = Agent.FixSpellingOfAgent(agentName);
                        obj.AgentName = agentName;
                        agentID = Agent.GetAgentIDByAgentName(obj);
                    }
                    obj.AgentID = agentID;
                    obj.AgentName = agentName;
                }
            }
            return obj;
        }

        private static PublicClasses.Objects_XL populateDealerData(PublicClasses.Objects_XL obj, DataRow ro, bool isDealerNumBlank, PublicClasses.ColumnValues cols)
        {
            if (string.IsNullOrEmpty(ro[0].ToString()))
            {
                isDealerNumBlank = true;
            }
            else
            {
                obj.DealerNo = ro[cols.DealerNo_Col].ToString();
            }
            obj.DealerName = ro[cols.DealerName_Col].ToString();

            obj.DealerID = Dealer.GetDealerID(obj.DealerNo, obj.DealerName, obj.ContractID, obj.FileType.Contains("AUTONATION"));
            if (obj.DealerID == null)
            {
                if (!Globals.IsTest)
                {
                    Globals.IsBadFile = true;
                }
            }

            if (isDealerNumBlank)
            {
                obj.DealerNo = Dealer.GetDealerNoWithDealerID(obj.DealerID);
            }
            else
            {
                obj.DealerNo = ro[cols.DealerNo_Col].ToString();
            }

            return obj;
        }

        private static bool setHasEffCancelDateColumn(List<string> columnNames)
        {
            foreach (string columnName in columnNames)
            {
                if (columnName.ToUpper().Contains("CANCEL DATE"))
                {
                    return true;
                }
            }
            return false;
        }

        static PublicClasses.Objects_XL SetCancelledValues(PublicClasses.Objects_XL obj, DataRow ro, bool hasAgentColumn, PublicClasses.ColumnValues cols)
        {
            if (hasAgentColumn)
            {
                obj.CancelPercent = StaticFuncts.parseDecimal(ro[cols.CancelPercent_Col]);
            }
            else
            {
                obj.CancelPercent = StaticFuncts.parseDecimal(ro[cols.CancelPercent_Col]);
                obj.DealerCost = StaticFuncts.parseDecimal(ro[cols.DealerCost_Col]);
            }
            return obj;
        }
    }
}

