﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Diagnostics;
using System.Data;


namespace CommisionImport
{
    public static class Globals
    {
        public static bool IsTest = false;
        public static bool IsDebug = false;
        public static string ConnString = string.Empty;
        public static string FilePath = string.Empty;
        public static List<string> TabsToIgnore = new();
        public static List<string> ValuesToRemoveFromAgentName = new();

        public static DateTime CommissionDate;
        public static int StartRow = 0;
        public static int AgentRow = 0;
        public static string AgentName = string.Empty;
        public static DateTime? CycleDate;
        public static List<string> FileTypes;
        public static int ColumnTotalField = 0;
        public static bool IsDateInFileName;

        // Values derived From AppSetting Paths
        public static string FilePathProcesing = string.Empty;
        public static string FileNameProcessing = string.Empty;
        public static string ProcessedFilePath = string.Empty;
        public static string ClaimDeductionFilePath = string.Empty;
        public static bool IsBadFile = false;

        public static string BadFilePath = string.Empty;
        public static string BadFileMessage = string.Empty;
        public static int BadFileCount = 0;
        public static List<string> BadFileNames = new();
        public static bool IsClaimDeductionFile = false;
        public static bool IsCommissionFile = false;
        public static int ClaimDeductFileCount = 0;
        public static List<string> ClaimDeductFileNames = new();
        public static bool SendEmail = false;

        //  Error Messages
        public static string SHEETAGENTID_NOTFOUND_BY_NAME = "File Header AgentID could not be determined: FileAgentName = ";
        public static string AGENTID_NOTFOUND_BY_NAME = "AgentID could not be determined, AgentName = ";
        public static string AGENTID_NOTFOUND_BY_ID = "AgentID could not be determined, AgentName = ";
        public static string AGENTID__NOTFOUND_BY_CONTACT = "AgentID could not be determined, Contact = ";
        public static string AGENTNAME_NOTFOUND_BY_AGENTID = "AgentName could not be found by AgentID = ";
        public static string AGENTID_NOTFOUND_BY_CONTRACTNO = "No AgentID found by querying with the Contract No = ";
        public static string AGENTID_NOTFOUND_BY_MGA_NOR_AGENT_TEMPLATE = "No AgentID found: Agent = {0}, MGA = {1}";

        public static string INVALID_DATE = "Invalid Date found in file. Value = ";
        public static string INVALID_DECIMAL = "Invalid Decimal found in file. Value = ";
        public static string INVALID_INTEGER = "Invalid Integer found in file. Value = ";

        public static void Initialize()
        {
            setIsTestAndConnString();
            setFilePaths();
            setTabsToIgnore();
            setStartRow();
            setFileTypes();
            setIsDateInFileName();
            setValuesToRemoveFromAgentName();
        }

        private static void setIsTestAndConnString()
        {
            string temp = ConfigurationManager.AppSettings.Get("Environment").ToUpper().Trim();
            Console.WriteLine("Pointing to: " + temp);
            Globals.IsTest = (temp == "TEST");
            if (Globals.IsTest)
            {
                ConnString = ConfigurationManager.ConnectionStrings["ConnStringTest"].ToString().Trim();
            }
            else
            {
                ConnString = ConfigurationManager.ConnectionStrings["ConnStringProd"].ToString().Trim();
            }

            if (Globals.IsTest)
            {
                IsDebug = bool.Parse(ConfigurationManager.AppSettings.Get("IsDebug"));
            }

            Globals.SendEmail = bool.Parse(ConfigurationManager.AppSettings.Get("SendEmail"));
        }

        public static void SetColumnTotalField(string totalColumnAppSettings)
        {
            ColumnTotalField = int.Parse(ConfigurationManager.AppSettings.Get(totalColumnAppSettings));
        }

        public static void setValuesToRemoveFromAgentName()
        {
            string values = ConfigurationManager.AppSettings.Get("RemoveFromAgentName");
            ValuesToRemoveFromAgentName = values.Split(",").ToList();
        }

        private static void setIsDateInFileName()
        {
            IsDateInFileName = bool.Parse(ConfigurationManager.AppSettings.Get("IsDateInFileName").Trim());
        }

        private static void setFileTypes()
        {
            string toSplit = ConfigurationManager.AppSettings.Get("FileTypes").ToUpper().Trim();
            FileTypes = toSplit.Split(",").ToList();
        }

        private static void killExcelProcess()
        {
            foreach (Process Proc in Process.GetProcesses())
            {
                if (Proc.ProcessName.Equals("EXCEL"))
                    Proc.Kill();
            }
        }
        private static void setFilePaths()
        {
            FilePath = ConfigurationManager.AppSettings.Get("PickupFilePath");
            BadFilePath = ConfigurationManager.AppSettings.Get("BadFilePath");
            ProcessedFilePath = ConfigurationManager.AppSettings.Get("ProcessedFilePath");
            ClaimDeductionFilePath = ConfigurationManager.AppSettings.Get("ClaimDeductionFilePath");
        }

        private static void setTabsToIgnore()
        {
            // Not Currently used.
            //string tabs = ConfigurationManager.AppSettings.Get("TabNamesToIgnore").ToUpper().Trim();
            //TabsToIgnore = tabs.Split(",").ToList<string>();            
        }

        private static void setStartRow()
        {
            string startrow = ConfigurationManager.AppSettings.Get("StartRow");
            Globals.StartRow = int.Parse(startrow);
            string agentRow = ConfigurationManager.AppSettings.Get("AgentRow");
            Globals.AgentRow = int.Parse(agentRow);
        }


    }
}
