﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommisionImport.SQL;
using System.Data;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using ExcelDataReader;
using System.Xml.Serialization;
using System.Globalization;


namespace CommisionImport
{
    public static class StaticFuncts
    {
        public static bool IsEmptyRow(DataRow ro)
        {
            // 2nd to last column is a Total value on empty row...

            for (int x = 0; x < Globals.ColumnTotalField; x++)
            {
                string temp = ro[x].ToString();
                if (!string.IsNullOrEmpty(temp))
                {
                    return false;
                }
            }
            return true;
        }

        public static string ReverseText(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static List<string> GetFiles()
        {
            List<string> files = Directory.GetFiles(Globals.FilePath, "*.xlsx").ToList<string>();
            return files;
        }

        public static void SetCDFiles()
        {
            Globals.ClaimDeductFileNames = Directory.GetFiles(Globals.ClaimDeductionFilePath, "*.xlsx").ToList<string>();
        }




        public static void SetBadFilesCount()
        {
            List<string> files = Directory.GetFiles(Globals.BadFilePath, "*.xlsx").ToList<string>();
            Globals.BadFileCount = files.Count;
        }



        public static void SetClaimDeductFileCount()
        {
            List<string> files = Directory.GetFiles(Globals.ClaimDeductionFilePath, "*.xlsx").ToList<string>();
            Globals.ClaimDeductFileCount = files.Count;
        }
        public static string GetFileType(string fileName)
        {
            fileName = fileName.ToUpper();
            foreach (string fileType in Globals.FileTypes)
            {
                if (fileName.Contains(fileType.ToUpper()))
                {
                    return fileType;
                }
            }
            return "Veritas";
        }

        public static void SetDateFromFileName(string fileName)
        {
            DateTime? dt = System.DateTime.Now;
            List<string> tempList = fileName.Split(" ").ToList();
            foreach (string temp in tempList)
            {
                if (isThisADate(temp))
                {
                    Globals.CycleDate = DateTime.Parse(temp);
                }
                else
                {
                    if (temp.Length == 6)
                    {
                        if (isAllDigits(temp))
                        {
                            string sMonth = temp.Substring(0, 2);
                            string sDay = temp.Substring(2, 2);
                            string sYear = temp.Substring(4, 2);
                            string date = sMonth + "/" + sDay + "/" + sYear;
                            Globals.CycleDate = DateTime.Parse(date);
                        }
                    }
                }
            }
        }

        public static bool isThisADate(string dateToCheck)
        {
            try
            {
                DateTime date = DateTime.Parse(dateToCheck);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool isAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (!char.IsDigit(c))
                    return false;
            }
            return true;
        }

        public static int parseInt(object value)
        {
            if (isCellEmptyOrNull(value))
            {
                return 0;
            }

            int parsed = 0;
            try
            {
                parsed = int.Parse(value.ToString());
                return parsed;
            }
            catch (Exception)
            {
                Globals.IsBadFile = true;
                Globals.BadFileMessage = Globals.INVALID_INTEGER + value;
            }
            return parsed;
        }

        public static DateTime? getDateTime(object value)
        {
            if (isCellEmptyOrNull(value))
            {
                return null;
            }

            DateTime? date = System.DateTime.Now;
            try
            {
                date = DateTime.Parse(value.ToString());
            }
            catch (Exception)
            {
                // This may be an OA Date:
                double oaDate = 0;
                bool success = double.TryParse(value.ToString(), out oaDate);
                if (success)
                {
                    date = DateTime.FromOADate(oaDate);
                }
                else
                {
                    Globals.IsBadFile = true;
                    Globals.BadFileMessage = Globals.INVALID_DATE + value.ToString();
                }
            }
            return date;
        }

        public static decimal parseDecimal(object value)
        {
            if (isCellEmptyOrNull(value))
            {
                return 0;
            }

            decimal parsed = 0;
            try
            {
                parsed = decimal.Parse(value.ToString());
                return parsed;
            }
            catch (Exception)
            {
                Globals.IsBadFile = true;
                Globals.BadFileMessage = Globals.INVALID_DECIMAL + value.ToString();
            }
            return 0;
        }

        public static DateTime? parseDateTime(double value)
        {
            try
            {
                DateTime parsed = DateTime.FromOADate(value);
                return parsed;
            }
            catch (Exception)
            {
                Globals.IsBadFile = true;
                Globals.BadFileMessage = Globals.INVALID_DATE + value.ToString();

            }
            return null;
        }

        private static bool isCellEmptyOrNull(object cellValue)
        {
            string cellString = cellValue.ToString();
            if (string.IsNullOrEmpty(cellString))
            { return true; }
            else
            { return false; }
        }

        public static DataSet ConvertExcelToDataSet(string filePath)
        {
            try
            {
                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                DataSet result = excelReader.AsDataSet();
                excelReader.Close();
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static int? GetContractID(string contractNum)
        {
            if (string.IsNullOrEmpty(contractNum))
            {
                return null;
            }

            contractNum = contractNum.Trim();
            SQL_Select ss = new();

            int? contractID = ss.GetContractID(contractNum);
            if (contractID == 0)
            {
                contractID = null;
            }
            return contractID;
        }

        public static PublicClasses.Objects_XL SetDateDataForRowItem(PublicClasses.Objects_XL obj)
        {
            if (obj.IsCancelled)
            {
                if (obj.FundedDate == null)
                {
                    obj.FundedDate = StaticFuncts.GetPaidDateForContractID(obj.ContractID);
                }

                if (obj.EffCancelDate == null)
                {
                    obj.FundedDate = StaticFuncts.GetEffCancelDateFromContractID(obj.ContractID);
                }

                if (obj.EffCancelDate == null)
                {
                    obj.IsValid = false;
                }

                if (obj.FundedDate == null)
                {
                    obj.IsValid = false;

                }
            }
            return obj;
        }

        public static bool ContainsNulls(PublicClasses.Objects_XL obj)
        {
            if (string.IsNullOrEmpty(obj.CustName) && !obj.SaleDate.HasValue && !obj.FundedDate.HasValue && !obj.DealerID.HasValue)
            {
                return true;
            }
            return false;
        }

        public static PublicClasses.Objects_XL FixAgentOrDealerNulls(PublicClasses.Objects_XL obj, PublicClasses.ColumnValues cols, PublicClasses.BooleanValues bv, DataRow ro, bool onlyFixDealer = false)
        {
            if (!string.IsNullOrEmpty(obj.AgentName))
            {
                string agentNameUp = obj.AgentName.ToUpper();
                if (agentNameUp.Contains("PEAKPERFORMANCE"))
                {
                    obj.AgentName.Replace("PEAKPERFORMANCE", "Peak Performance");
                }

                if (string.IsNullOrEmpty(obj.AgentName))
                {
                    SQL_Select ss = new();
                    if (obj.AgentID.HasValue)
                    {
                        obj.AgentName = ss.GetAgentNameByAgentID(obj.AgentID);
                    }
                }
            }

            if (obj.DealerID == null || obj.DealerID == 0)
            {
                obj = Dealer.GetDealerInfo(ro, obj, cols);
                if (!string.IsNullOrEmpty(obj.DealerName))
                {
                    if (cols.DealerName_Col > -1)
                    {
                        obj.DealerName = ro[cols.DealerName_Col].ToString();
                    }

                    if (!string.IsNullOrEmpty(obj.DealerName))
                    {
                        obj.DealerID = Dealer.GetDealerID(obj.DealerNo, obj.DealerName, obj.ContractID, true);
                    }
                }
            }
            if (onlyFixDealer)
            {
                return obj;
            }

            if (bv.HasMgaColumn && bv.HasAgentColumn)
            {
                if (obj.MGA_ID == null && obj.AgentID == null)
                {
                    string errorTemplate = Globals.AGENTID_NOTFOUND_BY_MGA_NOR_AGENT_TEMPLATE;
                    Globals.BadFileMessage = string.Format(errorTemplate, obj.AgentName, obj.MGA);
                    Globals.IsBadFile = true;
                }
            }
            else if (bv.HasAgentColumn)
            {
                if (obj.AgentID == null)
                {
                    obj.AgentID = Agent.GetAgentIDByAgentName(obj);
                    if (!obj.AgentID.HasValue)
                    {
                        Globals.IsBadFile = true;
                    }
                }
            }
            else if (bv.HasMgaColumn)
            {
                if (obj.SheetAgentID == null)
                {
                    Globals.BadFileMessage = Globals.SHEETAGENTID_NOTFOUND_BY_NAME + obj.SheetAgent;
                    Globals.IsBadFile = true;
                }
            }
            else // Has Neither MGA nor Agent columns
            {
                if (obj.SheetAgentID == null)
                {
                    Globals.BadFileMessage = Globals.SHEETAGENTID_NOTFOUND_BY_NAME + obj.SheetAgent;
                }
            }
            return obj;
        }

        public static bool SetIsFlorida()
        {
            string[] name = Globals.FileNameProcessing.Split(".");
            bool isFlorida = name[0].EndsWith(" FL");
            return isFlorida;
        }

        public static bool DoesRowHaveColumn(DataRow row, string colName)
        {
            colName = colName.ToUpper();
            List<Object> columns = row.ItemArray.ToList();
            foreach (object column in columns)
            {
                if (column.ToString().ToUpper().Trim() == colName)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool DoesSheetHaveColumn(DataTable dt, string colName)
        {
            colName = colName.ToUpper();
            if (colName == "AGENT")
            {
                if (Globals.FileNameProcessing.ToUpper().Contains("INSURED DEALER PROFITS"))
                {
                    return false;
                }
                else if (Globals.FileNameProcessing.ToUpper().Contains("VERO"))
                {
                    return false;
                }
            }

            int rowCounter = 0;
            foreach (DataRow row in dt.Rows)
            {
                List<Object> columns = row.ItemArray.ToList();
                foreach (object column in columns)
                {
                    if (column.ToString().ToUpper().Trim() == colName)
                    {
                        return true;
                    }
                }
                rowCounter++;
            }
            return false;
        }

        public static int GetColumnNumber(DataTable dt, string colName)
        {
            colName = colName.ToUpper();
            foreach (DataRow row in dt.Rows)
            {
                List<Object> columns = row.ItemArray.ToList();
                int counter = 0;
                foreach (object column in columns)
                {
                    if (column.ToString().ToUpper().Trim() == colName)
                    {
                        return counter;
                    }
                    counter++;
                }
            }
            return -1;
        }

        public static DateTime? GetEffCancelDateFromContractID(int? contractID)
        {
            DateTime? effCancelDate = null;
            SQL_Select ss = new();
            effCancelDate = ss.GetEffCancelDateFromContractID(contractID);
            return effCancelDate;
        }
        public static DateTime? GetPaidDateForContractID(int? contractID)
        {
            DateTime? fundedDate = null;
            SQL_Select ss = new();
            fundedDate = ss.GetPaidDateFromContract(contractID);
            return fundedDate;
        }
        public static List<string> GetColumnNames(DataRow ro)
        {
            List<string> columnNames = new();
            var roArray = ro.ItemArray;
            foreach (object ob in roArray)
            {
                if (!string.IsNullOrEmpty(ob.ToString()))
                {
                    columnNames.Add(ob.ToString().ToUpper());
                }
            }
            return columnNames;
        }

        public static void WriteErrorLog()
        {
            string path = Globals.BadFilePath + "ErrorLog.txt";

            string now = DateTime.Now.ToString("f",
                CultureInfo.CreateSpecificCulture("en-US"));

            string formattedLine = "{0} | {1} | {2}";
            string lineText = string.Format(formattedLine, now, Globals.FileNameProcessing, Globals.BadFileMessage);

            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(lineText);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(lineText);
                }
            }
        }

        public static void WriteLine(string message)
        {
            //if (Globals.IsDebug)
            //{
            Console.WriteLine(message);
            // }
        }

        // Used for Testing Purposes - Not Used Normally
        public static void ReadErrorLogThenWriteAgentNames()
        {
            string line;
            string path = Globals.BadFilePath + "ErrorLog.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            List<string> agentNames = new();

            while ((line = file.ReadLine()) != null)
            {
                if (line.Contains("AgentName ="))
                {
                    string[] splitString = line.Split("=");
                    // After the 2nd "=" sign, is the AgentName
                    if (splitString.Count() > 2)
                    {
                        string agentName = splitString[2].Trim();
                        if (!agentNames.Contains(agentName))
                        {
                            agentNames.Add(splitString[2]);
                        }
                    }
                }
            }

            file.Close();
            string newPath = Globals.BadFilePath + "AgentNames.txt";
            using (TextWriter tw = new StreamWriter(newPath))
            {
                foreach (String agentName in agentNames)
                    tw.WriteLine(agentName);
            }
        }


        public static void MoveFile()
        {
            if (Globals.IsBadFile)
            {
                string badFilePath = Globals.BadFilePath + Globals.FileNameProcessing;
                if (File.Exists(badFilePath))
                {
                    File.Delete(badFilePath);
                }
                try
                {
                    File.Move(Globals.FilePathProcesing, badFilePath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error MoveFile() = " + ex.Message);
                }
            }
            else if (Globals.IsClaimDeductionFile)
            {
                Globals.IsClaimDeductionFile = false;
                if (!Directory.Exists(Globals.ClaimDeductionFilePath))
                {
                    Directory.CreateDirectory(Globals.ClaimDeductionFilePath);
                }
                string processedFilePath = Globals.ClaimDeductionFilePath + Globals.FileNameProcessing;
                if (File.Exists(processedFilePath))
                {
                    File.Delete(processedFilePath);
                }
                try
                {
                    File.Move(Globals.FilePathProcesing, processedFilePath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error MoveFile() = " + ex.Message);
                }
            }
            else if (Globals.IsTest)
            {
                string processedFilePath = Globals.ProcessedFilePath + Globals.FileNameProcessing;
                if (File.Exists(processedFilePath))
                {
                    File.Delete(processedFilePath);
                }
                try
                {
                    File.Move(Globals.FilePathProcesing, processedFilePath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error MoveFile() = " + ex.Message);
                }
            }
        }

        // USED FOR TESTING.
        public static void WriteClassListToXMLFile(List<PublicClasses.Objects_XL> listObjs)
        {

            if (Globals.IsDebug && !Globals.IsBadFile)
            {
                if (string.IsNullOrEmpty(Globals.AgentName))
                {
                    Globals.AgentName = listObjs[0].SheetAgent;
                }
                string temp = Globals.AgentName.Replace(" ", "_");
                var filePath = Globals.FilePath + "Output_Class_" + temp + ".xml";
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                //create the serialiser to create the xml
                XmlSerializer serialiser = new XmlSerializer(typeof(List<PublicClasses.Objects_XL>));

                // Create the TextWriter for the serialiser to use
                TextWriter filestream = new StreamWriter(filePath);

                //write to the file
                serialiser.Serialize(filestream, listObjs);

                // Close the file
                filestream.Close();
            }
        }
    }
}




