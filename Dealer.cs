﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommisionImport.SQL;
using System.Data;

namespace CommisionImport
{
    public static class Dealer
    {
        public static int? GetDealerID(string dealerNum, string dealerName, int? contractID, bool isAutoNation)
        {
            int? dealerID = null;
            SQL_Select ss = new();

            dealerID = ss.GetDealerIDByDealerNum(dealerNum);
            if (dealerID == null)
            {
                dealerID = ss.GetDealerIDByDealerNameAndIsAutoNation(dealerName, isAutoNation);
            }

            if (!dealerID.HasValue)
            {
                dealerID = ss.GetDealerIDByDealerName(dealerNum);
            }

            return dealerID;
        }

        public static int? GetDealerIDWithDealerName(string dealerName)
        {
            int? dealerID = null;
            SQL_Select ss = new();

            dealerID = ss.GetDealerIDByDealerName(dealerName);
            if (!dealerID.HasValue)
            {
                dealerName = cleanDealerName(dealerName);
                dealerID = ss.GetDealerIDByDealerName(dealerName);
            }
            return dealerID;
        }
        public static int? GetDealerIDByContractID(int? contractID)
        {
            if (!contractID.HasValue)
            {
                return null;
            }

            int? dealerID = null;
            SQL_Select ss = new();

            dealerID = ss.GetDealerIDFromContractID(contractID);
            return dealerID;
        }


        public static int? GetDealerIDByDealerNum(string dealerNum)
        {
            int? dealerID = null;
            SQL_Select ss = new();

            dealerID = ss.GetDealerIDFromDealerNo(dealerNum);
            return dealerID;
        }

        public static string GetDealerNoFromContractID(int? contractID)
        {
            if (contractID == null || contractID == 0)
            {
                return null;
            }

            SQL_Select ss = new();
            string dealerNo = null;

            int? dealerID = ss.GetDealerIDFromContractID(contractID);
            if (dealerID.HasValue)
            {
                if (dealerID > 0)
                {
                    dealerNo = ss.GetDealerNoFromDealerID(dealerID);
                }
            }
            return dealerNo;

        }

        public static string GetDealerNameFromDealerID(int? dealerID)
        {
            SQL_Select ss = new();
            string dealerName = null;

            if (dealerID.HasValue)
            {
                if (dealerID > 0)
                {
                    dealerName = ss.GetDealerNameFromDealerID(dealerID);
                }
            }
            return dealerName;
        }


        public static string GetDealerNameFromDealerNo(string dealerNo)
        {
            if (string.IsNullOrEmpty(dealerNo))
            {
                return null;
            }
            string dealerName = string.Empty;
            SQL_Select ss = new();
            dealerName = ss.GetDealerNamebyDealerNo(dealerNo);
            return dealerName;
        }

        public static string GetDealerNoWithDealerID(int? dealerID)
        {
            SQL_Select ss = new();
            string dealerNo = ss.GetDealerNoFromDealerID(dealerID);
            return dealerNo;
        }

        public static string GetDealerNoWithDealerName(string dealerName)
        {
            SQL_Select ss = new();
            string dealerNo = ss.GetDealerNoByDealerName(dealerName);
            return dealerNo;
        }

        public static PublicClasses.Objects_XL GetDealerInfo(DataRow ro, PublicClasses.Objects_XL obj, PublicClasses.ColumnValues cols)
        {
            if (string.IsNullOrEmpty(obj.DealerName))
            {
                if (cols.DealerNo_Col > -1)
                {
                    string dealerNo = ro[cols.DealerNo_Col].ToString();
                    if (string.IsNullOrEmpty(dealerNo))
                    {
                        dealerNo = Dealer.GetDealerNoWithDealerName(obj.DealerName);
                    }
                    obj.DealerNo = dealerNo;
                }
            }

            if (obj.ContractID != null || obj.ContractID == 0)
            {
                obj.DealerNo = Dealer.GetDealerNoFromContractID(obj.ContractID);
            }
            if (cols.DealerName_Col > -1)
            {
                obj.DealerName = ro[cols.DealerName_Col].ToString();
            }
            else
            {
                obj.DealerName = Dealer.GetDealerNameFromDealerNo(obj.DealerNo);
            }

            if (cols.DealerNo_Col > -1)
            {
                string dealerNo = ro[cols.DealerNo_Col].ToString();
                if (string.IsNullOrEmpty(dealerNo))
                {
                    dealerNo = Dealer.GetDealerNoWithDealerName(obj.DealerName);
                }
                obj.DealerNo = dealerNo;
            }

            if (!string.IsNullOrEmpty(obj.DealerName))
            {
                obj.DealerID = Dealer.GetDealerIDWithDealerName(obj.DealerName);
            }


            return obj;
        }
        private static string cleanDealerName(string dealerName)
        {
            if (dealerName == null)
            {
                return null;
            }
            dealerName = dealerName.ToUpper();
            if (dealerName.Contains("EXTREME"))
            {
                dealerName = dealerName.Replace("INC.", "");
                dealerName = dealerName.Replace("INC", "");

            }
            else if (dealerName.Contains("CREDFIT"))
            {
                dealerName = dealerName.Replace("CREDFIT", "CREDIT");
            }

            return dealerName;
        }
    }
}
